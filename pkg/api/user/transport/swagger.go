package transport

import (
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// User model response
// swagger:response userResp
type swaggUserResponse struct {
	// in:body
	Body struct {
		*symbiomatique.User
	}
}

// Users model response
// swagger:response userListResp
type swaggUserListResponse struct {
	// in:body
	Body struct {
		Users []symbiomatique.User `json:"users"`
		Page  int                  `json:"page"`
	}
}
