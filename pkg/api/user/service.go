package user

import (
	"github.com/go-pg/pg/v9"
	"github.com/go-pg/pg/v9/orm"
	"github.com/labstack/echo/v4"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/user/platform/pgsql"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// Service represents user application interface
type Service interface {
	Create(echo.Context, symbiomatique.User) (*symbiomatique.User, error)
	List(echo.Context, *symbiomatique.Pagination) ([]symbiomatique.User, error)
	View(echo.Context, int) (*symbiomatique.User, error)
	Delete(echo.Context, int) error
	Update(echo.Context, *Update) (*symbiomatique.User, error)
}

// New creates new user application service
func New(db *pg.DB, udb UDB, rbac RBAC, sec Securer) *User {
	return &User{db: db, udb: udb, rbac: rbac, sec: sec}
}

// Initialize initializes User application service with defaults
func Initialize(db *pg.DB, rbac RBAC, sec Securer) *User {
	return New(db, pgsql.NewUser(), rbac, sec)
}

// User represents user application service
type User struct {
	db   *pg.DB
	udb  UDB
	rbac RBAC
	sec  Securer
}

// Securer represents security interface
type Securer interface {
	Hash(string) string
}

// UDB represents user repository interface
type UDB interface {
	Create(orm.DB, symbiomatique.User) (*symbiomatique.User, error)
	View(orm.DB, int) (*symbiomatique.User, error)
	List(orm.DB, *symbiomatique.ListQuery, *symbiomatique.Pagination) ([]symbiomatique.User, error)
	Update(orm.DB, *symbiomatique.User) error
	Delete(orm.DB, *symbiomatique.User) error
}

// RBAC represents role-based-access-control interface
type RBAC interface {
	User(echo.Context) *symbiomatique.AuthUser
	EnforceUser(echo.Context, int) error
	AccountCreate(echo.Context, symbiomatique.AccessRole, int, int) error
	IsLowerRole(echo.Context, symbiomatique.AccessRole) error
}
