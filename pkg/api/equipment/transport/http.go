package transport

import (
	"net/http"
	"strconv"

	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/equipment"

	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"

	"github.com/labstack/echo/v4"
)

// HTTP represents equipment http service
type HTTP struct {
	svc equipment.Service
}

// NewHTTP creates new equipment http service
func NewHTTP(svc equipment.Service, er *echo.Group) {
	h := HTTP{svc}
	ur := er.Group("/equipment")
	// swagger:route POST /v1/equipment equipment equipmentCreate
	// Creates new equipment.
	// responses:
	//  200: equipmentResp
	//  400: errMsg
	//  401: err
	//  403: errMsg
	//  500: err
	ur.POST("", h.create)

	// swagger:operation GET /v1/equipment equipment listEquipment
	// ---
	// summary: Returns list of equipment.
	// description: Returns list of equipment. Depending on the equipment role requesting it, it may return all equipment for SuperAdmin/Admin equipment, all company/location equipment for Company/Location admins, and an error for non-admin equipment.
	// parameters:
	// - name: limit
	//   in: query
	//   description: number of results
	//   type: int
	//   required: false
	// - name: page
	//   in: query
	//   description: page number
	//   type: int
	//   required: false
	// responses:
	//   "200":
	//     "$ref": "#/responses/equipmentListResp"
	//   "400":
	//     "$ref": "#/responses/errMsg"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	ur.GET("", h.list)

	// swagger:operation GET /v1/equipment/{id} equipment getEquipment
	// ---
	// summary: Returns a single equipment.
	// description: Returns a single equipment by its ID.
	// parameters:
	// - name: id
	//   in: path
	//   description: id of equipment
	//   type: int
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/responses/equipmentResp"
	//   "400":
	//     "$ref": "#/responses/err"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "404":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	ur.GET("/:id", h.view)

	// swagger:operation PATCH /v1/equipment/{id} equipment equipmentUpdate
	// ---
	// summary: Updates equipment's information.
	// description: Updates equipment's information.
	// parameters:
	// - name: id
	//   in: path
	//   description: id of equipment
	//   type: int
	//   required: true
	// - name: request
	//   in: body
	//   description: Request body
	//   required: true
	//   schema:
	//     "$ref": "#/definitions/equipmentUpdate"
	// responses:
	//   "200":
	//     "$ref": "#/responses/equipmentResp"
	//   "400":
	//     "$ref": "#/responses/errMsg"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	ur.PATCH("/:id", h.update)

	// swagger:operation PATCH /v1/equipment/{id} equipment equipmentSetOld
	// ---
	// summary: Set equipment as old
	// description: Updates equipment's contact information -> first name, last name, mobile, phone, address.
	// parameters:
	// - name: id
	//   in: path
	//   description: id of equipment
	//   type: int
	//   required: true
	// - name: request
	//   in: body
	//   description: Request body
	//   required: true
	//   schema:
	//     "$ref": "#/definitions/equipmentSetOld"
	// responses:
	//   "200":
	//     "$ref": "#/responses/equipmentResp"
	//   "400":
	//     "$ref": "#/responses/errMsg"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	ur.PATCH("/set-old/:id", h.setOld)

	// swagger:operation DELETE /v1/equipment/{id} equipment equipmentDelete
	// ---
	// summary: Deletes an equipment.
	// description: Deletes a equipment with requested ID.
	// parameters:
	// - name: id
	//   in: path
	//   description: id of equipment
	//   type: int
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/responses/ok"
	//   "400":
	//     "$ref": "#/responses/err"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	ur.DELETE("/:id", h.delete)

	// swagger:operation RE-AVERAGE-SCORE /v1/re-average-score/{id} equipment reAverageScore
	// ---
	// summary: Re-average all scores
	// description: Re-average all scores of an equipment
	// parameters:
	// - name: id
	//   in: path
	//   description: id of equipment
	//   type: int
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/responses/ok"
	//   "400":
	//     "$ref": "#/responses/err"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	ur.PATCH("/re-average-score/:id", h.reAverageScore)

	// swagger:operation CHECK-BEST-MATCH /v1/check-best-match equipment checkBestMatch
	// ---
	// summary: CheckBestMatch returns the best matching equipment
	// description: CheckBestMatch will check all compatible equipment data to find the best matching equipment
	// responses:
	//   "200":
	//     "$ref": "#/responses/ok"
	//   "400":
	//     "$ref": "#/responses/err"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	ur.PATCH("/check-best-match", h.checkBestMatch)
}

// Equipment create request
// swagger:model equipmentCreate
type createReq struct {
	Name  string  `json:"name" validate:"required,min=3,max=50"`
	Price float64 `json:"price" validate:"required"`
}

func (h *HTTP) create(c echo.Context) error {
	r := new(createReq)

	if err := c.Bind(r); err != nil {
		return err
	}

	eq, err := h.svc.Create(c, symbiomatique.Equipment{
		Name:  r.Name,
		IsOld: false,
		Price: r.Price,
	})

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, eq)
}

type listResponse struct {
	Equipment []symbiomatique.Equipment `json:"equipment"`
	Page      int                       `json:"page"`
}

func (h *HTTP) list(c echo.Context) error {
	p := new(symbiomatique.PaginationReq)
	if err := c.Bind(p); err != nil {
		return err
	}

	result, err := h.svc.List(c, p.Transform())

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, listResponse{result, p.Page})
}

func (h *HTTP) view(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return symbiomatique.ErrBadRequest
	}

	result, err := h.svc.View(c, id)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, result)
}

// Equipment update request
// swagger:model equipmentUpdate
type updateReq struct {
	ID    int     `json:"-"`
	Name  string  `json:"name" validate:"required,min=3,max=50"`
	IsOld bool    `json:"is_old"`
	Price float64 `json:"price" validate:"required"`
}

func (h *HTTP) update(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return symbiomatique.ErrBadRequest
	}

	req := new(updateReq)
	if err := c.Bind(req); err != nil {
		return err
	}

	eq, err := h.svc.Update(c, &equipment.Update{
		ID:    id,
		Name:  req.Name,
		IsOld: req.IsOld,
		Price: req.Price,
	})

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, eq)
}

func (h *HTTP) setOld(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return symbiomatique.ErrBadRequest
	}

	_, err = h.svc.Update(c, &equipment.Update{
		ID:    id,
		IsOld: false,
	})

	if err != nil {
		return err
	}

	return c.NoContent(http.StatusOK)
}

// Equipment check best match
// swagger:model checkBestMatch
type checkBestMatch struct {
	WantedAverageScore []float64 `json:"wanted_average_score"`
}

func (h *HTTP) checkBestMatch(c echo.Context) error {
	req := new(checkBestMatch)
	if err := c.Bind(req); err != nil {
		return err
	}

	eq, err := h.svc.CheckBestMatch(c, req.WantedAverageScore)

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, eq)
}

func (h *HTTP) reAverageScore(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return symbiomatique.ErrBadRequest
	}

	if err := h.svc.AverageScore(c, id); err != nil {
		return err
	}

	return c.NoContent(http.StatusOK)
}

func (h *HTTP) delete(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return symbiomatique.ErrBadRequest
	}

	if err := h.svc.Delete(c, id); err != nil {
		return err
	}

	return c.NoContent(http.StatusOK)
}
