package transport

import (
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// Equipment model response
// swagger:response equipmentResp
type swaggEquipmentResponse struct {
	// in:body
	Body struct {
		*symbiomatique.Equipment
	}
}

// Equipment model response
// swagger:response equipmentListResp
type swaggEquipmentListResponse struct {
	// in:body
	Body struct {
		Equipment []symbiomatique.Equipment `json:"equipment"`
		Page      int                       `json:"page"`
	}
}
