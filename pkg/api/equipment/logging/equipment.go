package equipment

import (
	"time"

	"github.com/labstack/echo/v4"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/equipment"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// New creates new equipment logging service
func New(svc equipment.Service, logger symbiomatique.Logger) *LogService {
	return &LogService{
		Service: svc,
		logger:  logger,
	}
}

// LogService represents equipment logging service
type LogService struct {
	equipment.Service
	logger symbiomatique.Logger
}

const name = "equipment"

// Create logging
func (ls *LogService) Create(c echo.Context, req symbiomatique.Equipment) (resp *symbiomatique.Equipment, err error) {
	defer func(begin time.Time) {
		ls.logger.Log(
			c,
			name, "Create equipment request", err,
			map[string]interface{}{
				"req":  req,
				"resp": resp,
				"took": time.Since(begin),
			},
		)
	}(time.Now())
	return ls.Service.Create(c, req)
}

// List logging
func (ls *LogService) List(c echo.Context, req *symbiomatique.Pagination) (resp []symbiomatique.Equipment, err error) {
	defer func(begin time.Time) {
		ls.logger.Log(
			c,
			name, "List equipment request", err,
			map[string]interface{}{
				"req":  req,
				"resp": resp,
				"took": time.Since(begin),
			},
		)
	}(time.Now())
	return ls.Service.List(c, req)
}

// View logging
func (ls *LogService) View(c echo.Context, req int) (resp *symbiomatique.Equipment, err error) {
	defer func(begin time.Time) {
		ls.logger.Log(
			c,
			name, "View equipment request", err,
			map[string]interface{}{
				"req":  req,
				"resp": resp,
				"took": time.Since(begin),
			},
		)
	}(time.Now())
	return ls.Service.View(c, req)
}

// Delete logging
func (ls *LogService) Delete(c echo.Context, req int) (err error) {
	defer func(begin time.Time) {
		ls.logger.Log(
			c,
			name, "Delete equipment request", err,
			map[string]interface{}{
				"req":  req,
				"took": time.Since(begin),
			},
		)
	}(time.Now())
	return ls.Service.Delete(c, req)
}

// Update logging
func (ls *LogService) Update(c echo.Context, req *equipment.Update) (resp *symbiomatique.Equipment, err error) {
	defer func(begin time.Time) {
		ls.logger.Log(
			c,
			name, "Update equipment request", err,
			map[string]interface{}{
				"req":  req,
				"resp": resp,
				"took": time.Since(begin),
			},
		)
	}(time.Now())
	return ls.Service.Update(c, req)
}
