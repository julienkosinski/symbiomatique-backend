// Package equipment contains equipment application services
package equipment

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// Create creates a new equipment account
func (e *Equipment) Create(c echo.Context, req symbiomatique.Equipment) (*symbiomatique.Equipment, error) {
	// Only Contributors can create new equipment
	if err := e.rbac.IsLowerRole(c, symbiomatique.ContributorRole); err != nil {
		return nil, err
	}
	return e.edb.Create(e.db, req)
}

// List returns list of equipment
func (e *Equipment) List(c echo.Context, p *symbiomatique.Pagination) ([]symbiomatique.Equipment, error) {
	return e.edb.List(e.db, p)
}

// View returns single equipment
func (e *Equipment) View(c echo.Context, id int) (*symbiomatique.Equipment, error) {
	return e.edb.View(e.db, id)
}

// AverageScore Route to re-average all scores
func (e *Equipment) AverageScore(c echo.Context, id int) error {
	// Only Contributors can validate a score re-averaging process
	if err := e.rbac.IsLowerRole(c, symbiomatique.ContributorRole); err != nil {
		return err
	}
	return e.edb.AverageScore(e.db, id)
}

// CheckBestMatch will return the best equipment matching the request
func (e *Equipment) CheckBestMatch(c echo.Context, wantedVectorAverageScore []float64) (*symbiomatique.Equipment, error) {
	// Only Contributors can validate a score re-averaging process
	if err := e.rbac.IsLowerRole(c, symbiomatique.ContributorRole); err != nil {
		return nil, err
	}
	return e.edb.CheckBestMatch(e.db, wantedVectorAverageScore)
}

// Delete deletes a equipment
func (e *Equipment) Delete(c echo.Context, id int) error {
	if err := e.rbac.IsLowerRole(c, symbiomatique.ContributorRole); err != nil {
		return nil
	}
	equipment, err := e.edb.View(e.db, id)
	if err != nil {
		return err
	}
	return e.edb.Delete(e.db, equipment)
}

// Update contains equipment's information used for updating
type Update struct {
	CreatedByID int
	ID          int
	Name        string
	IsOld       bool
	Price       float64
}

// Update updates equipment's contact information
func (e *Equipment) Update(c echo.Context, req *Update) (*symbiomatique.Equipment, error) {
	// If the user is not a contributor and not the creator of the equipment he is not authorized
	if err := e.rbac.IsLowerRole(c, symbiomatique.ContributorRole); err != nil {
		if req.CreatedByID != c.Get("id") {
			return nil, err
		}
	}

	if err := e.edb.Update(e.db, &symbiomatique.Equipment{
		Base:  symbiomatique.Base{ID: req.ID},
		Name:  req.Name,
		IsOld: req.IsOld,
		Price: req.Price,
	}); err != nil {
		return nil, err
	}

	return e.edb.View(e.db, req.ID)
}
