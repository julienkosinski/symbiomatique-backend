package pgsql

import (
	"math"
	"net/http"
	"strings"

	"github.com/go-pg/pg/v9"
	"github.com/go-pg/pg/v9/orm"
	"github.com/labstack/echo/v4"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// NewEquipment returns a new equipment database instance
func NewEquipment() *Equipment {
	return &Equipment{}
}

// Equipment represents the client for equipment table
type Equipment struct{}

// Custom errors
var (
	ErrAlreadyExists = echo.NewHTTPError(http.StatusInternalServerError, "Equipment already exists.")
)

// Create creates a new equipment on database
func (e *Equipment) Create(db orm.DB, equ symbiomatique.Equipment) (*symbiomatique.Equipment, error) {
	var equipment = new(symbiomatique.Equipment)
	err := db.Model(equipment).Where("lower(name) = ? and deleted_at is null",
		strings.ToLower(equ.Name)).Select()

	if err != nil && err != pg.ErrNoRows {
		return nil, ErrAlreadyExists
	}

	if err := db.Insert(&equ); err != nil {
		return nil, err
	}
	return &equ, nil
}

// View returns single equipment by ID
func (e *Equipment) View(db orm.DB, id int) (*symbiomatique.Equipment, error) {
	var equipment symbiomatique.Equipment
	err := db.Model(&equipment).Column("equipment.*").Relation("Components").Relation("Tags").Where("equipment.id = ? and equipment.deleted_at is null", id).First()
	if err != nil {
		return nil, err
	}

	return &equipment, nil
}

// Update updates equipment's contact info
func (e *Equipment) Update(db orm.DB, equipment *symbiomatique.Equipment) error {
	_, err := db.Model(equipment).WherePK().UpdateNotZero()
	return err
}

// List returns list of all equipments retrievable for the current equipment
func (e *Equipment) List(db orm.DB, p *symbiomatique.Pagination) ([]symbiomatique.Equipment, error) {
	var equipment []symbiomatique.Equipment
	q := db.Model(&equipment).Column("equipment.*").Relation("Components").Relation("Tags").Limit(p.Limit).Offset(p.Offset)
	if err := q.Select(); err != nil {
		return nil, err
	}
	return equipment, nil
}

// Delete sets deleted_at for a equipment
func (e *Equipment) Delete(db orm.DB, equipment *symbiomatique.Equipment) error {
	return db.Delete(equipment)
}

// AverageScore re-average all scores of an equipment
func (e *Equipment) AverageScore(db orm.DB, id int) error {
	var newAverageScores []symbiomatique.EquipmentTags
	sql1 := `SELECT "score"."tag_id", AVG("score"."score") AS "average_score", "score"."equipment_id" FROM "score"
			 WHERE ("score"."equipment_id" = ? and deleted_at is null)
			 GROUP BY "score"."tag_id", "score"."equipment_id"`

	_, err := db.Query(&newAverageScores, sql1, id)

	if err != nil {
		return err
	}

	for _, newAverageScore := range newAverageScores {
		isInserted, _ := db.Model(&newAverageScore).Where("equipment_id = ?equipment_id AND tag_id = ?tag_id").SelectOrInsert()

		if !isInserted {
			_, err = db.Model(&newAverageScore).Where("equipment_id = ?equipment_id AND tag_id = ?tag_id").WherePK().UpdateNotZero()
		}
	}

	return err
}

// CheckBestMatch will check all compatible equipment data to find the best matching equipment
func (e *Equipment) CheckBestMatch(db *pg.DB, wantedVectorAverageScore []float64) (*symbiomatique.Equipment, error) {
	var equipments []symbiomatique.Equipment
	var result symbiomatique.Equipment

	// Index AverageScore
	// 1) Categorize AverageScore with polarities (0% to 20%, 20 to 40%, 40% to 60%, 60% to 80%, 80% to 100%).
	// 2) Select all equipment within the polarity 1, 2, 3, 4, or 5 for each wantedVectorAverageScore
	// 3) Loop over like now

	// OR
	// Index AverageScore
	// Make only one big query with sub-query (is that possible ?)

	sql1 := `SELECT "equipment".* FROM "equipment" WHERE deleted_at is null`
	_, err := db.Query(&equipments, sql1)
	if err != nil {
		return nil, err
	}

	var prevDistance float64
	var currentAverageResultForTag float64
	var currentDistance float64

	for _, equipment := range equipments {
		currentAverageResultForTag = 0
		currentDistance = 0
		var equipmentTags []symbiomatique.EquipmentTags
		// Get average scores ordered by each tags
		sql2 := `SELECT "equipment_tags".* FROM "equipment_tags" WHERE "equipment_tags"."equipment_id" = ? ORDER BY "equipment_tags"."tag_id"`
		_, err := db.Query(&equipmentTags, sql2, equipment.ID)
		if err != nil {
			return nil, err
		}

		for indexEquipmentTag, equipmentTag := range equipmentTags {
			currentAverageResultForTag = math.Pow(equipmentTag.AverageScore-wantedVectorAverageScore[indexEquipmentTag], 2)
			currentDistance += currentAverageResultForTag
		}

		currentDistance = math.Sqrt(currentDistance)
		if currentDistance < prevDistance || prevDistance == 0 {
			result = equipment
			prevDistance = currentDistance
		}
	}

	return &result, nil
}
