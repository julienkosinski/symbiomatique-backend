package equipment_test

import (
	"github.com/go-pg/pg/v9/orm"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/equipment"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/mock"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/mock/mockdb"
	symbiomatique "gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
	"testing"
)

func TestCreate(t *testing.T) {
	type args struct {
		c   echo.Context
		req symbiomatique.Equipment
	}
	cases := []struct {
		name     string
		args     args
		wantErr  bool
		wantData *symbiomatique.Equipment
		edb      *mockdb.Equipment
		rbac     *mock.RBAC
	}{{
		name: "Fail on is lower role",
		rbac: &mock.RBAC{
			IsLowerRoleFn: func(echo.Context, symbiomatique.AccessRole) error {
				return symbiomatique.ErrGeneric
			}},
		wantErr: true,
		args: args{req: symbiomatique.Equipment{
			Name:  "Gigabyte GA-Z170X Gaming 7",
			IsOld: false,
			Price: 500,
		}},
	},
		{
			name: "Success",
			rbac: &mock.RBAC{
				IsLowerRoleFn: func(echo.Context, symbiomatique.AccessRole) error {
					return nil
				}},
			wantErr: false,
			args: args{req: symbiomatique.Equipment{
				Name:  "Gigabyte GA-Z170X Gaming 7",
				IsOld: false,
				Price: 500,
			}},
			edb: &mockdb.Equipment{
				CreateFn: func(db orm.DB, u symbiomatique.Equipment) (*symbiomatique.Equipment, error) {
					u.CreatedAt = mock.TestTime(2000)
					u.UpdatedAt = mock.TestTime(2000)
					u.Base.ID = 1
					return &u, nil
				},
			},
			wantData: &symbiomatique.Equipment{
				Base: symbiomatique.Base{
					ID:        1,
					CreatedAt: mock.TestTime(2000),
					UpdatedAt: mock.TestTime(2000),
				},
				Name:  "Gigabyte GA-Z170X Gaming 7",
				IsOld: false,
				Price: 500,
			}}}
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			s := equipment.New(nil, tt.edb, tt.rbac)
			eq, err := s.Create(tt.args.c, tt.args.req)
			assert.Equal(t, tt.wantErr, err != nil)
			assert.Equal(t, tt.wantData, eq)
		})
	}
}

func TestView(t *testing.T) {
	type args struct {
		c  echo.Context
		id int
	}
	cases := []struct {
		name     string
		args     args
		wantData *symbiomatique.Equipment
		wantErr  error
		edb      *mockdb.Equipment
	}{
		{
			name: "Success",
			args: args{id: 1},
			wantData: &symbiomatique.Equipment{
				Base: symbiomatique.Base{
					ID:        1,
					CreatedAt: mock.TestTime(2000),
					UpdatedAt: mock.TestTime(2000),
				},
				Name:  "Gigabyte GA-Z170X Gaming 7",
				IsOld: false,
				Price: 500,
			},
			edb: &mockdb.Equipment{
				ViewFn: func(db orm.DB, id int) (*symbiomatique.Equipment, error) {
					if id == 1 {
						return &symbiomatique.Equipment{
							Base: symbiomatique.Base{
								ID:        1,
								CreatedAt: mock.TestTime(2000),
								UpdatedAt: mock.TestTime(2000),
							},
							Name:  "Gigabyte GA-Z170X Gaming 7",
							IsOld: false,
							Price: 500,
						}, nil
					}
					return nil, nil
				}},
		},
	}
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			s := equipment.New(nil, tt.edb, nil)
			eq, err := s.View(tt.args.c, tt.args.id)
			assert.Equal(t, tt.wantData, eq)
			assert.Equal(t, tt.wantErr, err)
		})
	}
}

func TestList(t *testing.T) {
	type args struct {
		c   echo.Context
		pgn *symbiomatique.Pagination
	}
	cases := []struct {
		name     string
		args     args
		wantData []symbiomatique.Equipment
		wantErr  bool
		edb      *mockdb.Equipment
	}{
		{
			name: "Success",
			args: args{c: nil, pgn: &symbiomatique.Pagination{
				Limit:  100,
				Offset: 200,
			}},
			edb: &mockdb.Equipment{
				ListFn: func(orm.DB, *symbiomatique.Pagination) ([]symbiomatique.Equipment, error) {
					return []symbiomatique.Equipment{
						{
							Base: symbiomatique.Base{
								ID:        1,
								CreatedAt: mock.TestTime(1999),
								UpdatedAt: mock.TestTime(2000),
							},
							Name:  "Gigabyte GA-Z170X Gaming 7",
							IsOld: false,
							Price: 500,
						},
						{
							Base: symbiomatique.Base{
								ID:        2,
								CreatedAt: mock.TestTime(2001),
								UpdatedAt: mock.TestTime(2002),
							},
							Name:  "Gigabyte GA-Z170X Gaming 7",
							IsOld: false,
							Price: 500,
						},
					}, nil
				}},
			wantData: []symbiomatique.Equipment{
				{
					Base: symbiomatique.Base{
						ID:        1,
						CreatedAt: mock.TestTime(1999),
						UpdatedAt: mock.TestTime(2000),
					},
					Name:  "Gigabyte GA-Z170X Gaming 7",
					IsOld: false,
					Price: 500,
				},
				{
					Base: symbiomatique.Base{
						ID:        2,
						CreatedAt: mock.TestTime(2001),
						UpdatedAt: mock.TestTime(2002),
					},
					Name:  "Gigabyte GA-Z170X Gaming 7",
					IsOld: false,
					Price: 500,
				}},
		},
	}
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			s := equipment.New(nil, tt.edb, nil)
			eqs, err := s.List(tt.args.c, tt.args.pgn)
			assert.Equal(t, tt.wantData, eqs)
			assert.Equal(t, tt.wantErr, err != nil)
		})
	}

}

// TODO : Do not use merge struct for Updates. Make it the same as user and then delete utl/structs/merge.go

/*
func TestDelete(t *testing.T) {
	type args struct {
		c  echo.Context
		id int
	}
	cases := []struct {
		name    string
		args    args
		wantErr error
		edb     *mockdb.Equipment
		rbac    *mock.RBAC
	}{
		{
			name:    "Fail on ViewEquipment",
			args:    args{id: 1},
			wantErr: symbiomatique.ErrGeneric,
			edb: &mockdb.Equipment{
				ViewFn: func(db orm.DB, id int) (*symbiomatique.Equipment, error) {
					if id != 1 {
						return nil, nil
					}
					return nil, symbiomatique.ErrGeneric
				},
			},
		},
		{
			name: "Fail on RBAC",
			args: args{id: 1},
			edb: &mockdb.Equipment{
				ViewFn: func(db orm.DB, id int) (*symbiomatique.Equipment, error) {
					return &symbiomatique.Equipment{
						Base: symbiomatique.Base{
							ID:        id,
							CreatedAt: mock.TestTime(1999),
							UpdatedAt: mock.TestTime(2000),
						},
						Name:  "Gigabyte GA-Z170X Gaming 7",
						IsOld: false,
						Price: 500,
					}, nil
				},
			},
			rbac: &mock.RBAC{
				IsLowerRoleFn: func(echo.Context, symbiomatique.AccessRole) error {
					return symbiomatique.ErrGeneric
				}},
			wantErr: symbiomatique.ErrGeneric,
		},
		{
			name: "Success",
			args: args{id: 1},
			edb: &mockdb.Equipment{
				ViewFn: func(db orm.DB, id int) (*symbiomatique.Equipment, error) {
					return &symbiomatique.Equipment{
						Base: symbiomatique.Base{
							ID:        id,
							CreatedAt: mock.TestTime(1999),
							UpdatedAt: mock.TestTime(2000),
						},
						Name:  "Gigabyte GA-Z170X Gaming 7",
						IsOld: false,
						Price: 500,
					}, nil
				},
				DeleteFn: func(db orm.DB, eq *symbiomatique.Equipment) error {
					return nil
				},
			},
			rbac: &mock.RBAC{
				IsLowerRoleFn: func(echo.Context, symbiomatique.AccessRole) error {
					return nil
				}},
		},
	}
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			s := equipment.New(nil, tt.edb, tt.rbac)
			err := s.Delete(tt.args.c, tt.args.id)
			if err != tt.wantErr {
				t.Errorf("Expected error %v, received %v", tt.wantErr, err)
			}
		})
	}
}

func TestUpdate(t *testing.T) {
	type args struct {
		c   echo.Context
		upd *equipment.Update
	}
	cases := []struct {
		name     string
		args     args
		wantData *symbiomatique.Equipment
		wantErr  error
		edb      *mockdb.Equipment
		rbac     *mock.RBAC
	}{
		{
			name: "Fail on RBAC",
			args: args{upd: &equipment.Update{
				ID: 1,
			}},
			rbac: &mock.RBAC{
				EnforceUserFn: func(c echo.Context, id int) error {
					return symbiomatique.ErrGeneric
				}},
			wantErr: symbiomatique.ErrGeneric,
		},
		{
			name: "Fail on Update",
			args: args{upd: &equipment.Update{
				ID: 1,
			}},
			rbac: &mock.RBAC{
				EnforceUserFn: func(c echo.Context, id int) error {
					return nil
				}},
			wantErr: symbiomatique.ErrGeneric,
			edb: &mockdb.Equipment{
				ViewFn: func(db orm.DB, id int) (*symbiomatique.Equipment, error) {
					return &symbiomatique.Equipment{
						Base: symbiomatique.Base{
							ID:        1,
							CreatedAt: mock.TestTime(1990),
							UpdatedAt: mock.TestTime(1991),
						},
						Name:  "Gigabyte GA-Z170X Gaming 7",
						IsOld: false,
						Price: 500,
					}, nil
				},
				UpdateFn: func(db orm.DB, eq *symbiomatique.Equipment) error {
					return symbiomatique.ErrGeneric
				},
			},
		},
		{
			name: "Success",
			args: args{upd: &equipment.Update{
				ID:        1,
				Name:  "Gigabyte GA-Z170X Gaming 7",
				IsOld: false,
				Price: 500,
			}},
			rbac: &mock.RBAC{
				EnforceUserFn: func(c echo.Context, id int) error {
					return nil
				}},
			wantData: &symbiomatique.Equipment{
				Base: symbiomatique.Base{
					ID:        1,
					CreatedAt: mock.TestTime(1990),
					UpdatedAt: mock.TestTime(2000),
				},
				Name:  "Gigabyte GA-Z170X Gaming 7",
				IsOld: false,
				Price: 500,
			},
			edb: &mockdb.Equipment{
				ViewFn: func(db orm.DB, id int) (*symbiomatique.Equipment, error) {
					return &symbiomatique.Equipment{
						Base: symbiomatique.Base{
							ID:        1,
							CreatedAt: mock.TestTime(1990),
							UpdatedAt: mock.TestTime(2000),
						},
						Name:  "Gigabyte GA-Z170X Gaming 7",
						IsOld: false,
						Price: 500,
					}, nil
				},
				UpdateFn: func(db orm.DB, eq *symbiomatique.Equipment) error {
					eq.UpdatedAt = mock.TestTime(2000)
					return nil
				},
			},
		},
	}
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			s := equipment.New(nil, tt.edb, tt.rbac)
			eq, err := s.Update(tt.args.c, tt.args.upd)
			assert.Equal(t, tt.wantData, eq)
			assert.Equal(t, tt.wantErr, err)
		})
	}
}
*/

func TestInitialize(t *testing.T) {
	u := equipment.Initialize(nil, nil)
	if u == nil {
		t.Error("Equipment service not initialized")
	}
}
