package equipment

import (
	"github.com/go-pg/pg/v9"
	"github.com/go-pg/pg/v9/orm"
	"github.com/labstack/echo/v4"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/equipment/platform/pgsql"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// Service represents equipment application interface
type Service interface {
	Create(echo.Context, symbiomatique.Equipment) (*symbiomatique.Equipment, error)
	List(echo.Context, *symbiomatique.Pagination) ([]symbiomatique.Equipment, error)
	View(echo.Context, int) (*symbiomatique.Equipment, error)
	Delete(echo.Context, int) error
	Update(echo.Context, *Update) (*symbiomatique.Equipment, error)
	AverageScore(echo.Context, int) error
	CheckBestMatch(echo.Context, []float64) (*symbiomatique.Equipment, error)
}

// New creates new equipment application service
func New(db *pg.DB, edb EDB, rbac RBAC) *Equipment {
	return &Equipment{db: db, edb: edb, rbac: rbac}
}

// Initialize initalizes Equipment application service with defaults
func Initialize(db *pg.DB, rbac RBAC) *Equipment {
	return New(db, pgsql.NewEquipment(), rbac)
}

// Equipment represents equipment application service
type Equipment struct {
	db   *pg.DB
	edb  EDB
	rbac RBAC
}

// EDB represents equipment repository interface
type EDB interface {
	Create(orm.DB, symbiomatique.Equipment) (*symbiomatique.Equipment, error)
	View(orm.DB, int) (*symbiomatique.Equipment, error)
	List(orm.DB, *symbiomatique.Pagination) ([]symbiomatique.Equipment, error)
	Update(orm.DB, *symbiomatique.Equipment) error
	Delete(orm.DB, *symbiomatique.Equipment) error
	AverageScore(orm.DB, int) error
	CheckBestMatch(*pg.DB, []float64) (*symbiomatique.Equipment, error)
}

// RBAC represents role-based-access-control interface
type RBAC interface {
	IsLowerRole(echo.Context, symbiomatique.AccessRole) error
	EnforceUser(echo.Context, int) error
}
