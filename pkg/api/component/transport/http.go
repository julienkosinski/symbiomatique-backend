package transport

import (
	"net/http"
	"strconv"

	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/component"

	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"

	"github.com/labstack/echo/v4"
)

// HTTP represents component http service
type HTTP struct {
	svc component.Service
}

// NewHTTP creates new component http service
func NewHTTP(svc component.Service, er *echo.Group) {
	h := HTTP{svc}
	comp := er.Group("/component")
	// swagger:route POST /v1/component component componentCreate
	// Creates new component.
	// responses:
	//  200: componentResp
	//  400: errMsg
	//  401: err
	//  403: errMsg
	//  500: err
	comp.POST("", h.create)

	// swagger:operation GET /v1/component component listComponent
	// ---
	// summary: Returns list of components.
	// description: Returns list of component. Depending on the component role requesting it, it may return all component for SuperAdmin/Admin component, all company/location component for Company/Location admins, and an error for non-admin component.
	// parameters:
	// - name: limit
	//   in: query
	//   description: number of results
	//   type: int
	//   required: false
	// - name: page
	//   in: query
	//   description: page number
	//   type: int
	//   required: false
	// responses:
	//   "200":
	//     "$ref": "#/responses/componentListResp"
	//   "400":
	//     "$ref": "#/responses/errMsg"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	comp.GET("", h.list)

	// swagger:operation GET /v1/component/{id} component getComponent
	// ---
	// summary: Returns a single component.
	// description: Returns a single component by its ID.
	// parameters:
	// - name: id
	//   in: path
	//   description: id of component
	//   type: int
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/responses/componentResp"
	//   "400":
	//     "$ref": "#/responses/err"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "404":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	comp.GET("/:id", h.view)

	// swagger:operation PATCH /v1/component/{id} component componentUpdate
	// ---
	// summary: Updates component's information.
	// description: Updates component's information.
	// parameters:
	// - name: id
	//   in: path
	//   description: id of component
	//   type: int
	//   required: true
	// - name: request
	//   in: body
	//   description: Request body
	//   required: true
	//   schema:
	//     "$ref": "#/definitions/componentUpdate"
	// responses:
	//   "200":
	//     "$ref": "#/responses/componentResp"
	//   "400":
	//     "$ref": "#/responses/errMsg"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	comp.PATCH("/:id", h.update)

	// swagger:operation DELETE /v1/component/{id} component componentDelete
	// ---
	// summary: Deletes a component.
	// description: Deletes a component with requested ID.
	// parameters:
	// - name: id
	//   in: path
	//   description: id of component
	//   type: int
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/responses/ok"
	//   "400":
	//     "$ref": "#/responses/err"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	comp.DELETE("/:id", h.delete)
}

// Component create request
// swagger:model componentCreate
type createReq struct {
	Name string `json:"name" validate:"required,min=3,max=50"`
}

func (h *HTTP) create(c echo.Context) error {
	r := new(createReq)

	if err := c.Bind(r); err != nil {
		return err
	}

	usr, err := h.svc.Create(c, symbiomatique.Component{
		Name: r.Name,
	})

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, usr)
}

type listResponse struct {
	Component []symbiomatique.Component `json:"component"`
	Page      int                       `json:"page"`
}

func (h *HTTP) list(c echo.Context) error {
	p := new(symbiomatique.PaginationReq)
	if err := c.Bind(p); err != nil {
		return err
	}

	result, err := h.svc.List(c, p.Transform())

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, listResponse{result, p.Page})
}

func (h *HTTP) view(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return symbiomatique.ErrBadRequest
	}

	result, err := h.svc.View(c, id)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, result)
}

// Component update request
// swagger:model componentUpdate
type updateReq struct {
	ID   int    `json:"-"`
	Name string `json:"name" validate:"required,min=3,max=50"`
}

func (h *HTTP) update(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return symbiomatique.ErrBadRequest
	}

	req := new(updateReq)
	if err := c.Bind(req); err != nil {
		return err
	}

	usr, err := h.svc.Update(c, &component.Update{
		ID:   id,
		Name: req.Name,
	})

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, usr)
}

func (h *HTTP) delete(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return symbiomatique.ErrBadRequest
	}

	if err := h.svc.Delete(c, id); err != nil {
		return err
	}

	return c.NoContent(http.StatusOK)
}
