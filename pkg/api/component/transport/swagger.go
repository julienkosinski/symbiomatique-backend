package transport

import (
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// Component model response
// swagger:response componentResp
type swaggComponentResponse struct {
	// in:body
	Body struct {
		*symbiomatique.Component
	}
}

// Components model response
// swagger:response componentListResp
type swaggComponentListResponse struct {
	// in:body
	Body struct {
		Components []symbiomatique.Component `json:"components"`
		Page       int                       `json:"page"`
	}
}
