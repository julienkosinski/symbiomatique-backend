package component

import (
	"github.com/go-pg/pg/v9"
	"github.com/go-pg/pg/v9/orm"
	"github.com/labstack/echo/v4"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/component/platform/pgsql"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// Service represents component application interface
type Service interface {
	Create(echo.Context, symbiomatique.Component) (*symbiomatique.Component, error)
	List(echo.Context, *symbiomatique.Pagination) ([]symbiomatique.Component, error)
	View(echo.Context, int) (*symbiomatique.Component, error)
	Delete(echo.Context, int) error
	Update(echo.Context, *Update) (*symbiomatique.Component, error)
}

// New creates new component application service
func New(db *pg.DB, cdb CDB, rbac RBAC) *Component {
	return &Component{db: db, cdb: cdb, rbac: rbac}
}

// Initialize initializes Component application service with defaults
func Initialize(db *pg.DB, rbac RBAC) *Component {
	return New(db, pgsql.NewComponent(), rbac)
}

// Component represents component application service
type Component struct {
	db   *pg.DB
	cdb  CDB
	rbac RBAC
}

// CDB represents component repository interface
type CDB interface {
	Create(orm.DB, symbiomatique.Component) (*symbiomatique.Component, error)
	View(orm.DB, int) (*symbiomatique.Component, error)
	List(orm.DB, *symbiomatique.Pagination) ([]symbiomatique.Component, error)
	Update(orm.DB, *symbiomatique.Component) error
	Delete(orm.DB, *symbiomatique.Component) error
}

// RBAC represents role-based-access-control interface
type RBAC interface {
	IsLowerRole(echo.Context, symbiomatique.AccessRole) error
	EnforceUser(echo.Context, int) error
}
