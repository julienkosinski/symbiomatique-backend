// Package component contains component application services
package component

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// Create creates a new component account
func (co *Component) Create(c echo.Context, req symbiomatique.Component) (*symbiomatique.Component, error) {
	if err := co.rbac.IsLowerRole(c, symbiomatique.ContributorRole); err != nil {
		return nil, err
	}

	return co.cdb.Create(co.db, req)
}

// List returns list of component
func (co *Component) List(c echo.Context, p *symbiomatique.Pagination) ([]symbiomatique.Component, error) {
	return co.cdb.List(co.db, p)
}

// View returns single component
func (co *Component) View(c echo.Context, id int) (*symbiomatique.Component, error) {
	return co.cdb.View(co.db, id)
}

// Delete deletes a component
func (co *Component) Delete(c echo.Context, id int) error {
	if err := co.rbac.IsLowerRole(c, symbiomatique.ContributorRole); err != nil {
		return err
	}

	component, err := co.cdb.View(co.db, id)
	if err != nil {
		return err
	}
	return co.cdb.Delete(co.db, component)
}

// Update contains component's information used for updating
type Update struct {
	ID   int
	Name string
}

// Update updates component's contact information
func (co *Component) Update(c echo.Context, req *Update) (*symbiomatique.Component, error) {
	if err := co.rbac.IsLowerRole(c, symbiomatique.ContributorRole); err != nil {
		return nil, err
	}

	if err := co.cdb.Update(co.db, &symbiomatique.Component{
		Base: symbiomatique.Base{ID: req.ID},
		Name: req.Name,
	}); err != nil {
		return nil, err
	}

	return co.cdb.View(co.db, req.ID)
}
