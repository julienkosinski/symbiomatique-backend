package pgsql

import (
	"net/http"
	"strings"

	"github.com/go-pg/pg/v9"
	"github.com/go-pg/pg/v9/orm"
	"github.com/labstack/echo/v4"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// NewComponent returns a new component database instance
func NewComponent() *Component {
	return &Component{}
}

// Component represents the client for component table
type Component struct{}

// Custom errors
var (
	ErrAlreadyExists = echo.NewHTTPError(http.StatusInternalServerError, "Component already exists.")
)

// Create creates a new component on database
func (c *Component) Create(db orm.DB, com symbiomatique.Component) (*symbiomatique.Component, error) {
	var component = new(symbiomatique.Component)
	err := db.Model(component).Where("lower(type) = ? and deleted_at is null",
		strings.ToLower(com.Name)).Select()

	if err != nil && err != pg.ErrNoRows {
		return nil, ErrAlreadyExists

	}

	if err := db.Insert(&com); err != nil {
		return nil, err
	}
	return &com, nil
}

// View returns single component by ID
func (c *Component) View(db orm.DB, id int) (*symbiomatique.Component, error) {
	var component symbiomatique.Component
	err := db.Model(&component).Column("component.*").Relation("ListEquipment").Where("component.id = ? and component.deleted_at is null", id).First()
	if err != nil {
		return nil, err
	}

	return &component, nil
}

// Update updates component's contact info
func (c *Component) Update(db orm.DB, component *symbiomatique.Component) error {
	_, err := db.Model(component).WherePK().UpdateNotZero()
	return err
}

// List returns list of all components retrievable for the current component
func (c *Component) List(db orm.DB, p *symbiomatique.Pagination) ([]symbiomatique.Component, error) {
	var component []symbiomatique.Component
	q := db.Model(&component).Column("component.*").Relation("ListEquipment").Limit(p.Limit).Offset(p.Offset)
	if err := q.Select(); err != nil {
		return nil, err
	}
	return component, nil
}

// Delete sets deleted_at for a component
func (c *Component) Delete(db orm.DB, component *symbiomatique.Component) error {
	return db.Delete(component)
}
