package auth

import (
	"github.com/go-pg/pg/v9"
	"github.com/go-pg/pg/v9/orm"
	"github.com/labstack/echo/v4"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/auth/platform/pgsql"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// New creates new auth service
func New(db *pg.DB, udb UserDB, j TokenGenerator, sec Securer, rbac RBAC) *Auth {
	return &Auth{
		db:   db,
		udb:  udb,
		tg:   j,
		sec:  sec,
		rbac: rbac,
	}
}

// Initialize initializes auth application service
func Initialize(db *pg.DB, j TokenGenerator, sec Securer, rbac RBAC) *Auth {
	return New(db, pgsql.NewUser(), j, sec, rbac)
}

// Service represents auth service interface
type Service interface {
	Authenticate(echo.Context, string, string) (*symbiomatique.AuthToken, error)
	Refresh(echo.Context, string) (*symbiomatique.RefreshToken, error)
	Me(echo.Context) (*symbiomatique.User, error)
}

// Auth represents auth application service
type Auth struct {
	db   *pg.DB
	udb  UserDB
	tg   TokenGenerator
	sec  Securer
	rbac RBAC
}

// UserDB represents user repository interface
type UserDB interface {
	View(orm.DB, int) (*symbiomatique.User, error)
	FindByUsername(orm.DB, string) (*symbiomatique.User, error)
	FindByToken(orm.DB, string) (*symbiomatique.User, error)
	Update(orm.DB, *symbiomatique.User) error
}

// TokenGenerator represents token generator (jwt) interface
type TokenGenerator interface {
	GenerateToken(*symbiomatique.User) (string, string, error)
}

// Securer represents security interface
type Securer interface {
	HashMatchesPassword(string, string) bool
	Token(string) string
}

// RBAC represents role-based-access-control interface
type RBAC interface {
	User(echo.Context) *symbiomatique.AuthUser
}
