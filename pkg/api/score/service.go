package Score

import (
	"github.com/go-pg/pg/v9"
	"github.com/go-pg/pg/v9/orm"
	"github.com/labstack/echo/v4"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/score/platform/pgsql"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// Service represents Score application interface
type Service interface {
	Create(echo.Context, symbiomatique.Score) (*symbiomatique.Score, error)
	List(echo.Context, *symbiomatique.Pagination) ([]symbiomatique.Score, error)
	View(echo.Context, int) (*symbiomatique.Score, error)
	Delete(echo.Context, int) error
	Update(echo.Context, *Update) (*symbiomatique.Score, error)
}

// New creates new Score application service
func New(db *pg.DB, edb EDB, rbac RBAC) *Score {
	return &Score{db: db, edb: edb, rbac: rbac}
}

// Initialize initializes Score application service with defaults
func Initialize(db *pg.DB, rbac RBAC) *Score {
	return New(db, pgsql.NewScore(), rbac)
}

// Score represents Score application service
type Score struct {
	db   *pg.DB
	edb  EDB
	rbac RBAC
}

// EDB represents Score repository interface
type EDB interface {
	Create(orm.DB, symbiomatique.Score) (*symbiomatique.Score, error)
	View(orm.DB, int) (*symbiomatique.Score, error)
	List(orm.DB, *symbiomatique.Pagination) ([]symbiomatique.Score, error)
	Update(orm.DB, *symbiomatique.Score) error
	Delete(orm.DB, *symbiomatique.Score) error
}

// RBAC represents role-based-access-control interface
type RBAC interface {
	IsLowerRole(echo.Context, symbiomatique.AccessRole) error
	EnforceUser(echo.Context, int) error
}
