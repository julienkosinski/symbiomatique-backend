package Score

import (
	"time"

	"github.com/labstack/echo/v4"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/score"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// New creates new Score logging service
func New(svc Score.Service, logger symbiomatique.Logger) *LogService {
	return &LogService{
		Service: svc,
		logger:  logger,
	}
}

// LogService represents Score logging service
type LogService struct {
	Score.Service
	logger symbiomatique.Logger
}

const name = "Score"

// Create logging
func (ls *LogService) Create(c echo.Context, req symbiomatique.Score) (resp *symbiomatique.Score, err error) {
	defer func(begin time.Time) {
		ls.logger.Log(
			c,
			name, "Create Score request", err,
			map[string]interface{}{
				"req":  req,
				"resp": resp,
				"took": time.Since(begin),
			},
		)
	}(time.Now())
	return ls.Service.Create(c, req)
}

// List logging
func (ls *LogService) List(c echo.Context, req *symbiomatique.Pagination) (resp []symbiomatique.Score, err error) {
	defer func(begin time.Time) {
		ls.logger.Log(
			c,
			name, "List Score request", err,
			map[string]interface{}{
				"req":  req,
				"resp": resp,
				"took": time.Since(begin),
			},
		)
	}(time.Now())
	return ls.Service.List(c, req)
}

// View logging
func (ls *LogService) View(c echo.Context, req int) (resp *symbiomatique.Score, err error) {
	defer func(begin time.Time) {
		ls.logger.Log(
			c,
			name, "View Score request", err,
			map[string]interface{}{
				"req":  req,
				"resp": resp,
				"took": time.Since(begin),
			},
		)
	}(time.Now())
	return ls.Service.View(c, req)
}

// Delete logging
func (ls *LogService) Delete(c echo.Context, req int) (err error) {
	defer func(begin time.Time) {
		ls.logger.Log(
			c,
			name, "Delete Score request", err,
			map[string]interface{}{
				"req":  req,
				"took": time.Since(begin),
			},
		)
	}(time.Now())
	return ls.Service.Delete(c, req)
}

// Update logging
func (ls *LogService) Update(c echo.Context, req *Score.Update) (resp *symbiomatique.Score, err error) {
	defer func(begin time.Time) {
		ls.logger.Log(
			c,
			name, "Update Score request", err,
			map[string]interface{}{
				"req":  req,
				"resp": resp,
				"took": time.Since(begin),
			},
		)
	}(time.Now())
	return ls.Service.Update(c, req)
}
