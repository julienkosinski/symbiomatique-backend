package transport

import (
	"net/http"
	"strconv"

	Score "gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/score"

	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"

	"github.com/labstack/echo/v4"
)

// HTTP represents Score http service
type HTTP struct {
	svc Score.Service
}

// NewHTTP creates new Score http service
func NewHTTP(svc Score.Service, er *echo.Group) {
	h := HTTP{svc}
	ur := er.Group("/score")
	// swagger:route POST /v1/score score scoreCreate
	// Creates new Score.
	// responses:
	//  200: ScoreResp
	//  400: errMsg
	//  401: err
	//  403: errMsg
	//  500: err
	ur.POST("", h.create)

	// swagger:operation GET /v1/score score listScore
	// ---
	// summary: Returns list of Score.
	// description: Returns list of Score. Depending on the Score role requesting it, it may return all Score for SuperAdmin/Admin Score, all company/location Score for Company/Location admins, and an error for non-admin Score.
	// parameters:
	// - name: limit
	//   in: query
	//   description: number of results
	//   type: int
	//   required: false
	// - name: page
	//   in: query
	//   description: page number
	//   type: int
	//   required: false
	// responses:
	//   "200":
	//     "$ref": "#/responses/ScoreListResp"
	//   "400":
	//     "$ref": "#/responses/errMsg"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	ur.GET("", h.list)

	// swagger:operation GET /v1/score/{id} score getScore
	// ---
	// summary: Returns a single Score.
	// description: Returns a single Score by its ID.
	// parameters:
	// - name: id
	//   in: path
	//   description: id of Score
	//   type: int
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/responses/ScoreResp"
	//   "400":
	//     "$ref": "#/responses/err"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "404":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	ur.GET("/:id", h.view)

	// swagger:operation PATCH /v1/score/{id} score scoreUpdate
	// ---
	// summary: Updates Score's information.
	// description: Updates Score's information.
	// parameters:
	// - name: id
	//   in: path
	//   description: id of Score
	//   type: int
	//   required: true
	// - name: request
	//   in: body
	//   description: Request body
	//   required: true
	//   schema:
	//     "$ref": "#/definitions/ScoreUpdate"
	// responses:
	//   "200":
	//     "$ref": "#/responses/ScoreResp"
	//   "400":
	//     "$ref": "#/responses/errMsg"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	ur.PATCH("/:id", h.update)

	// swagger:operation DELETE /v1/score/{id} score scoreDelete
	// ---
	// summary: Deletes a Score.
	// description: Deletes a Score with requested ID.
	// parameters:
	// - name: id
	//   in: path
	//   description: id of Score
	//   type: int
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/responses/ok"
	//   "400":
	//     "$ref": "#/responses/err"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	ur.DELETE("/:id", h.delete)
}

// Score create request
// swagger:model ScoreCreate
type createReq struct {
	EquipmentID int     `json:"equipment_id" validate:"required"`
	TagID       int     `json:"component_id" validate:"required"`
	Score       float64 `json:"score" validate:"required"`
}

func (h *HTTP) create(c echo.Context) error {
	r := new(createReq)

	if err := c.Bind(r); err != nil {

		return err
	}

	usr, err := h.svc.Create(c, symbiomatique.Score{
		EquipmentID: r.EquipmentID,
		TagID:       r.TagID,
		Score:       r.Score,
	})

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, usr)
}

type listResponse struct {
	Score []symbiomatique.Score `json:"Score"`
	Page  int                   `json:"page"`
}

func (h *HTTP) list(c echo.Context) error {
	p := new(symbiomatique.PaginationReq)
	if err := c.Bind(p); err != nil {
		return err
	}

	result, err := h.svc.List(c, p.Transform())

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, listResponse{result, p.Page})
}

func (h *HTTP) view(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return symbiomatique.ErrBadRequest
	}

	result, err := h.svc.View(c, id)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, result)
}

// Score update request
// swagger:model ScoreUpdate
type updateReq struct {
	ID          int     `json:"-"`
	EquipmentID int     `json:"equipment_id"`
	TagID       int     `json:"tag_id"`
	Score       float64 `json:"score,omitempty"`
}

func (h *HTTP) update(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return symbiomatique.ErrBadRequest
	}

	req := new(updateReq)
	if err := c.Bind(req); err != nil {
		return err
	}

	usr, err := h.svc.Update(c, &Score.Update{
		ID:          id,
		EquipmentID: req.EquipmentID,
		TagID:       req.TagID,
		Score:       req.Score,
	})

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, usr)
}

func (h *HTTP) delete(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return symbiomatique.ErrBadRequest
	}

	if err := h.svc.Delete(c, id); err != nil {
		return err
	}

	return c.NoContent(http.StatusOK)
}
