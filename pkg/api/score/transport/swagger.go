package transport

import (
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// Score model response
// swagger:response scoreResp
type swaggScoreResponse struct {
	// in:body
	Body struct {
		*symbiomatique.Score
	}
}

// Scores model response
// swagger:response scoreListResp
type swaggScoreListResponse struct {
	// in:body
	Body struct {
		Scores []symbiomatique.Score `json:"scores"`
		Page   int                   `json:"page"`
	}
}
