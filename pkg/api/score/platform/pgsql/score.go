package pgsql

import (
	"net/http"

	"github.com/go-pg/pg/v9"
	"github.com/go-pg/pg/v9/orm"
	"github.com/labstack/echo/v4"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// NewScore returns a new Score database instance
func NewScore() *Score {
	return &Score{}
}

// Score represents the client for Score table
type Score struct{}

// Custom errors
var (
	ErrAlreadyExists = echo.NewHTTPError(http.StatusInternalServerError, "Score already exists.")
)

// Create creates a new Score on database
func (s *Score) Create(db orm.DB, eqfe symbiomatique.Score) (*symbiomatique.Score, error) {
	var Score = new(symbiomatique.Score)
	err := db.Model(Score).Where("equipment_id = ? and deleted_at is null",
		eqfe.EquipmentID).Where("tag_id = ? and deleted_at is null",
		eqfe.TagID).Select()

	if err != nil && err != pg.ErrNoRows {
		return nil, ErrAlreadyExists

	}

	if err := db.Insert(&eqfe); err != nil {
		return nil, err
	}
	return &eqfe, nil
}

// View returns single Score by ID
func (s *Score) View(db orm.DB, id int) (*symbiomatique.Score, error) {
	var Score = new(symbiomatique.Score)
	sql := `SELECT "score".* FROM "score" WHERE ("score"."id" = ? and deleted_at is null)`
	_, err := db.QueryOne(Score, sql, id)
	if err != nil {
		return nil, err
	}

	return Score, nil
}

// Update updates Score's info
func (s *Score) Update(db orm.DB, Score *symbiomatique.Score) error {
	_, err := db.Model(Score).WherePK().UpdateNotZero()
	return err
}

// List returns list of all Scores retrievable for the current Score
func (s *Score) List(db orm.DB, p *symbiomatique.Pagination) ([]symbiomatique.Score, error) {
	var Score []symbiomatique.Score
	q := db.Model(&Score).Column("score.*").Limit(p.Limit).Offset(p.Offset)
	if err := q.Select(); err != nil {
		return nil, err
	}
	return Score, nil
}

// Delete sets deleted_at for a Score
func (s *Score) Delete(db orm.DB, Score *symbiomatique.Score) error {
	return db.Delete(Score)
}
