// Package Score contains Score application services
package Score

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// Create creates a new Score account
func (e *Score) Create(c echo.Context, req symbiomatique.Score) (*symbiomatique.Score, error) {
	if err := e.rbac.IsLowerRole(c, symbiomatique.ContributorRole); err != nil {
		return nil, err
	}

	return e.edb.Create(e.db, req)
}

// List returns list of Score
func (e *Score) List(c echo.Context, p *symbiomatique.Pagination) ([]symbiomatique.Score, error) {
	if err := e.rbac.IsLowerRole(c, symbiomatique.AdminRole); err != nil {
		return nil, err
	}

	return e.edb.List(e.db, p)
}

// View returns single Score
func (e *Score) View(c echo.Context, id int) (*symbiomatique.Score, error) {
	if err := e.rbac.IsLowerRole(c, symbiomatique.ContributorRole); err != nil {
		return nil, err
	}

	Score, err := e.edb.View(e.db, id)
	if err != nil {
		return nil, err
	}
	if err := e.rbac.EnforceUser(c, Score.CreatedByID); err != nil {
		return nil, err
	}
	return e.edb.View(e.db, id)
}

// Delete deletes a Score
func (e *Score) Delete(c echo.Context, id int) error {
	if err := e.rbac.IsLowerRole(c, symbiomatique.ContributorRole); err != nil {
		return err
	}

	Score, err := e.edb.View(e.db, id)
	if err != nil {
		return err
	}
	if err := e.rbac.EnforceUser(c, Score.CreatedByID); err != nil {
		return err
	}
	return e.edb.Delete(e.db, Score)
}

// Update contains Score's information used for updating
type Update struct {
	ID          int
	EquipmentID int
	TagID       int
	Score       float64
}

// Update updates Score's contact information
func (e *Score) Update(c echo.Context, req *Update) (*symbiomatique.Score, error) {
	if err := e.rbac.IsLowerRole(c, symbiomatique.ContributorRole); err != nil {
		return nil, err
	}

	Score, err := e.edb.View(e.db, req.ID)
	if err != nil {
		return nil, err
	}
	if err := e.rbac.EnforceUser(c, Score.CreatedByID); err != nil {
		return nil, err
	}

	if err := e.edb.Update(e.db, &symbiomatique.Score{
		Base:        symbiomatique.Base{ID: req.ID},
		EquipmentID: req.EquipmentID,
		TagID:       req.TagID,
		Score:       req.Score,
	}); err != nil {
		return nil, err
	}

	return Score, nil
}
