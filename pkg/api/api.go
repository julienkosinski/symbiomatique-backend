// Based on Gorsk, https://github.com/ribice/gorsk
// Below is the original Copyright
// Copyright 2017 Emir Ribic. All rights reserved.
// Use of this source code is governed by an MIT-style
// license that can be found in the ORIGINAL_LICENSE file.

// Symbiomatique - Make the good choices for your equipment.
//
// API Docs for Symbiomatique
//
//     Terms Of Service:  N/A
//     Schemes: http
//     Version: 0.0.1
//     Contact: Julien Kosinski <julien.kosinski@gmail.com> https://julienkosinski.com
//     Host: localhost:8080
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//     Security:
//     - bearer: []
//
//     SecurityDefinitions:
//     bearer:
//          type: apiKey
//          name: Authorization
//          in: header
//
// swagger:meta
package api

import (
	"crypto/sha1"

	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/zlog"

	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/auth"
	al "gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/auth/logging"
	at "gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/auth/transport"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/component"
	cl "gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/component/logging"
	ct "gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/component/transport"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/equipment"
	el "gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/equipment/logging"
	et "gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/equipment/transport"
	equipmentComponent "gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/equipment_components"
	ecl "gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/equipment_components/logging"
	ect "gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/equipment_components/transport"
	equipmentTags "gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/equipment_tags"
	efl "gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/equipment_tags/logging"
	eft "gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/equipment_tags/transport"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/password"
	pl "gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/password/logging"
	pt "gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/password/transport"
	score "gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/score"
	ql "gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/score/logging"
	qt "gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/score/transport"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/tag"
	fl "gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/tag/logging"
	ft "gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/tag/transport"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/user"
	ul "gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/user/logging"
	ut "gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/user/transport"

	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/config"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/middleware/jwt"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/postgres"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/rbac"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/secure"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/server"
)

// Start starts the API service
func Start(cfg *config.Configuration) error {
	db, err := postgres.New(cfg.DB.PSN, cfg.DB.Timeout, cfg.DB.LogQueries)
	if err != nil {
		return err
	}

	sec := secure.New(cfg.App.MinPasswordStr, sha1.New())
	rbac := rbac.New()
	jwt := jwt.New(cfg.JWT.Secret, cfg.JWT.SigningAlgorithm, cfg.JWT.Duration)
	log := zlog.New()

	e := server.New()
	e.Static("/swaggerui", cfg.App.SwaggerUIPath)

	at.NewHTTP(al.New(auth.Initialize(db, jwt, sec, rbac), log), e, jwt.MWFunc())

	v1 := e.Group("/v1")
	v1.Use(jwt.MWFunc())

	ut.NewHTTP(ul.New(user.Initialize(db, rbac, sec), log), v1)
	pt.NewHTTP(pl.New(password.Initialize(db, rbac, sec), log), v1)
	et.NewHTTP(el.New(equipment.Initialize(db, rbac), log), v1)
	ct.NewHTTP(cl.New(component.Initialize(db, rbac), log), v1)
	ft.NewHTTP(fl.New(tag.Initialize(db, rbac), log), v1)
	qt.NewHTTP(ql.New(score.Initialize(db, rbac), log), v1)
	ect.NewHTTP(ecl.New(equipmentComponent.Initialize(db, rbac), log), v1)
	eft.NewHTTP(efl.New(equipmentTags.Initialize(db, rbac), log), v1)

	server.Start(e, &server.Config{
		Port:                cfg.Server.Port,
		ReadTimeoutSeconds:  cfg.Server.ReadTimeout,
		WriteTimeoutSeconds: cfg.Server.WriteTimeout,
		Debug:               cfg.Server.Debug,
	})

	return nil
}
