package transport

import (
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// Tag model response
// swagger:response tagResp
type swaggTagResponse struct {
	// in:body
	Body struct {
		*symbiomatique.Tag
	}
}

// Tags model response
// swagger:response tagListResp
type swaggTagListResponse struct {
	// in:body
	Body struct {
		Tags []symbiomatique.Tag `json:"tags"`
		Page int                 `json:"page"`
	}
}
