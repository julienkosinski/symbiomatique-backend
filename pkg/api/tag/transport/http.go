package transport

import (
	"net/http"
	"strconv"

	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/tag"

	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"

	"github.com/labstack/echo/v4"
)

// HTTP represents tag http service
type HTTP struct {
	svc tag.Service
}

// NewHTTP creates new tag http service
func NewHTTP(svc tag.Service, er *echo.Group) {
	h := HTTP{svc}
	ur := er.Group("/tag")
	// swagger:route POST /v1/tag tag tagCreate
	// Creates new tag account.
	// responses:
	//  200: tagResp
	//  400: errMsg
	//  401: err
	//  403: errMsg
	//  500: err
	ur.POST("", h.create)

	// swagger:operation GET /v1/tag tag listTag
	// ---
	// summary: Returns list of tag.
	// description: Returns list of tag. Depending on the tag role requesting it, it may return all tag for SuperAdmin/Admin tag, all company/location tag for Company/Location admins, and an error for non-admin tag.
	// parameters:
	// - name: limit
	//   in: query
	//   description: number of results
	//   type: int
	//   required: false
	// - name: page
	//   in: query
	//   description: page number
	//   type: int
	//   required: false
	// responses:
	//   "200":
	//     "$ref": "#/responses/tagListResp"
	//   "400":
	//     "$ref": "#/responses/errMsg"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	ur.GET("", h.list)

	// swagger:operation GET /v1/tag/{id} tag getTag
	// ---
	// summary: Returns a single tag.
	// description: Returns a single tag by its ID.
	// parameters:
	// - name: id
	//   in: path
	//   description: id of tag
	//   type: int
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/responses/tagResp"
	//   "400":
	//     "$ref": "#/responses/err"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "404":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	ur.GET("/:id", h.view)

	// swagger:operation PATCH /v1/tag/{id} tag tagUpdate
	// ---
	// summary: Updates tag's information.
	// description: Updates tag's information.
	// parameters:
	// - name: id
	//   in: path
	//   description: id of tag
	//   type: int
	//   required: true
	// - name: request
	//   in: body
	//   description: Request body
	//   required: true
	//   schema:
	//     "$ref": "#/definitions/tagUpdate"
	// responses:
	//   "200":
	//     "$ref": "#/responses/tagResp"
	//   "400":
	//     "$ref": "#/responses/errMsg"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	ur.PATCH("/:id", h.update)

	// swagger:operation DELETE /v1/tag/{id} tag tagDelete
	// ---
	// summary: Deletes a tag.
	// description: Deletes a tag with requested ID.
	// parameters:
	// - name: id
	//   in: path
	//   description: id of tag
	//   type: int
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/responses/ok"
	//   "400":
	//     "$ref": "#/responses/err"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	ur.DELETE("/:id", h.delete)
}

// Tag create request
// swagger:model tagCreate
type createReq struct {
	Name string `json:"name" validate:"required,min=3,max=50"`
}

func (h *HTTP) create(c echo.Context) error {
	r := new(createReq)

	if err := c.Bind(r); err != nil {

		return err
	}

	usr, err := h.svc.Create(c, symbiomatique.Tag{
		Name: r.Name,
	})

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, usr)
}

type listResponse struct {
	Tag  []symbiomatique.Tag `json:"tag"`
	Page int                 `json:"page"`
}

func (h *HTTP) list(c echo.Context) error {
	p := new(symbiomatique.PaginationReq)
	if err := c.Bind(p); err != nil {
		return err
	}

	result, err := h.svc.List(c, p.Transform())

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, listResponse{result, p.Page})
}

func (h *HTTP) view(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return symbiomatique.ErrBadRequest
	}

	result, err := h.svc.View(c, id)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, result)
}

// Tag update request
// swagger:model tagUpdate
type updateReq struct {
	ID   int    `json:"-"`
	Name string `json:"name" validate:"required,min=3,max=50"`
}

func (h *HTTP) update(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return symbiomatique.ErrBadRequest
	}

	req := new(updateReq)
	if err := c.Bind(req); err != nil {
		return err
	}

	usr, err := h.svc.Update(c, &tag.Update{
		ID:   id,
		Name: req.Name,
	})

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, usr)
}

func (h *HTTP) delete(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return symbiomatique.ErrBadRequest
	}

	if err := h.svc.Delete(c, id); err != nil {
		return err
	}

	return c.NoContent(http.StatusOK)
}
