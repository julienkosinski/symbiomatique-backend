// Package tag contains tag application services
package tag

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// Create creates a new tag account
func (t *Tag) Create(c echo.Context, req symbiomatique.Tag) (*symbiomatique.Tag, error) {
	return t.tdb.Create(t.db, req)
}

// List returns list of tag
func (t *Tag) List(c echo.Context, p *symbiomatique.Pagination) ([]symbiomatique.Tag, error) {
	return t.tdb.List(t.db, p)
}

// View returns single tag
func (t *Tag) View(c echo.Context, id int) (*symbiomatique.Tag, error) {
	return t.tdb.View(t.db, id)
}

// Delete deletes a tag
func (t *Tag) Delete(c echo.Context, id int) error {
	tag, err := t.tdb.View(t.db, id)
	if err != nil {
		return err
	}
	return t.tdb.Delete(t.db, tag)
}

// Update contains tag's information used for updating
type Update struct {
	ID   int
	Name string
}

// Update updates tag's information
func (t *Tag) Update(c echo.Context, req *Update) (*symbiomatique.Tag, error) {
	if err := t.tdb.Update(t.db, &symbiomatique.Tag{
		Base: symbiomatique.Base{ID: req.ID},
		Name: req.Name,
	}); err != nil {
		return nil, err
	}

	return t.tdb.View(t.db, req.ID)
}
