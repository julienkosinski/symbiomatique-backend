package tag

import (
	"github.com/go-pg/pg/v9"
	"github.com/go-pg/pg/v9/orm"
	"github.com/labstack/echo/v4"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/tag/platform/pgsql"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// Service represents tag application interface
type Service interface {
	Create(echo.Context, symbiomatique.Tag) (*symbiomatique.Tag, error)
	List(echo.Context, *symbiomatique.Pagination) ([]symbiomatique.Tag, error)
	View(echo.Context, int) (*symbiomatique.Tag, error)
	Delete(echo.Context, int) error
	Update(echo.Context, *Update) (*symbiomatique.Tag, error)
}

// New creates new tag application service
func New(db *pg.DB, tdb TDB, rbac RBAC) *Tag {
	return &Tag{db: db, tdb: tdb, rbac: rbac}
}

// Initialize initializes Tag application service with defaults
func Initialize(db *pg.DB, rbac RBAC) *Tag {
	return New(db, pgsql.NewTag(), rbac)
}

// Tag represents tag application service
type Tag struct {
	db   *pg.DB
	tdb  TDB
	rbac RBAC
}

// TDB represents tag repository interface
type TDB interface {
	Create(orm.DB, symbiomatique.Tag) (*symbiomatique.Tag, error)
	View(orm.DB, int) (*symbiomatique.Tag, error)
	List(orm.DB, *symbiomatique.Pagination) ([]symbiomatique.Tag, error)
	Update(orm.DB, *symbiomatique.Tag) error
	Delete(orm.DB, *symbiomatique.Tag) error
}

// RBAC represents role-based-access-control interface
type RBAC interface {
	IsLowerRole(echo.Context, symbiomatique.AccessRole) error
	EnforceUser(echo.Context, int) error
}
