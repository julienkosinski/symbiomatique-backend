package tag

import (
	"time"

	"github.com/labstack/echo/v4"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/tag"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// New creates new tag logging service
func New(svc tag.Service, logger symbiomatique.Logger) *LogService {
	return &LogService{
		Service: svc,
		logger:  logger,
	}
}

// LogService represents tag logging service
type LogService struct {
	tag.Service
	logger symbiomatique.Logger
}

const name = "tag"

// Create logging
func (ls *LogService) Create(c echo.Context, req symbiomatique.Tag) (resp *symbiomatique.Tag, err error) {
	defer func(begin time.Time) {
		ls.logger.Log(
			c,
			name, "Create tag request", err,
			map[string]interface{}{
				"req":  req,
				"resp": resp,
				"took": time.Since(begin),
			},
		)
	}(time.Now())
	return ls.Service.Create(c, req)
}

// List logging
func (ls *LogService) List(c echo.Context, req *symbiomatique.Pagination) (resp []symbiomatique.Tag, err error) {
	defer func(begin time.Time) {
		ls.logger.Log(
			c,
			name, "List tag request", err,
			map[string]interface{}{
				"req":  req,
				"resp": resp,
				"took": time.Since(begin),
			},
		)
	}(time.Now())
	return ls.Service.List(c, req)
}

// View logging
func (ls *LogService) View(c echo.Context, req int) (resp *symbiomatique.Tag, err error) {
	defer func(begin time.Time) {
		ls.logger.Log(
			c,
			name, "View tag request", err,
			map[string]interface{}{
				"req":  req,
				"resp": resp,
				"took": time.Since(begin),
			},
		)
	}(time.Now())
	return ls.Service.View(c, req)
}

// Delete logging
func (ls *LogService) Delete(c echo.Context, req int) (err error) {
	defer func(begin time.Time) {
		ls.logger.Log(
			c,
			name, "Delete tag request", err,
			map[string]interface{}{
				"req":  req,
				"took": time.Since(begin),
			},
		)
	}(time.Now())
	return ls.Service.Delete(c, req)
}

// Update logging
func (ls *LogService) Update(c echo.Context, req *tag.Update) (resp *symbiomatique.Tag, err error) {
	defer func(begin time.Time) {
		ls.logger.Log(
			c,
			name, "Update tag request", err,
			map[string]interface{}{
				"req":  req,
				"resp": resp,
				"took": time.Since(begin),
			},
		)
	}(time.Now())
	return ls.Service.Update(c, req)
}
