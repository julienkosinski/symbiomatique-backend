package pgsql

import (
	"net/http"
	"strings"

	"github.com/go-pg/pg/v9"
	"github.com/go-pg/pg/v9/orm"
	"github.com/labstack/echo/v4"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// NewTag returns a new tag database instance
func NewTag() *Tag {
	return &Tag{}
}

// Tag represents the client for tag table
type Tag struct{}

// Custom errors
var (
	ErrAlreadyExists = echo.NewHTTPError(http.StatusInternalServerError, "Tag already exists.")
)

// Create creates a new tag on database
func (t *Tag) Create(db orm.DB, equ symbiomatique.Tag) (*symbiomatique.Tag, error) {
	var tag = new(symbiomatique.Tag)
	err := db.Model(tag).Where("lower(name) = ? and deleted_at is null",
		strings.ToLower(equ.Name)).Select()

	if err != nil && err != pg.ErrNoRows {
		return nil, ErrAlreadyExists

	}

	if err := db.Insert(&equ); err != nil {
		return nil, err
	}
	return &equ, nil
}

// View returns single tag by ID
func (t *Tag) View(db orm.DB, id int) (*symbiomatique.Tag, error) {
	var tag = new(symbiomatique.Tag)
	sql := `SELECT "tag".* FROM "tag" WHERE ("tag"."id" = ? and deleted_at is null)`
	_, err := db.QueryOne(tag, sql, id)
	if err != nil {
		return nil, err
	}

	return tag, nil
}

// Update updates tag's contact info
func (t *Tag) Update(db orm.DB, tag *symbiomatique.Tag) error {
	_, err := db.Model(tag).WherePK().UpdateNotZero()
	return err
}

// List returns list of all tags retrievable for the current tag
func (t *Tag) List(db orm.DB, p *symbiomatique.Pagination) ([]symbiomatique.Tag, error) {
	var tag []symbiomatique.Tag
	q := db.Model(&tag).Column("tag.*").Limit(p.Limit).Offset(p.Offset)
	if err := q.Select(); err != nil {
		return nil, err
	}
	return tag, nil
}

// Delete sets deleted_at for a tag
func (t *Tag) Delete(db orm.DB, tag *symbiomatique.Tag) error {
	return db.Delete(tag)
}
