// Package equipmentTags contains equipmentTags application services
package equipmentTags

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// Create creates a new equipmentTags
func (e *EquipmentTags) Create(c echo.Context, req symbiomatique.EquipmentTags) (*symbiomatique.EquipmentTags, error) {
	if err := e.rbac.IsLowerRole(c, symbiomatique.ContributorRole); err != nil {
		return nil, err
	}

	return e.edb.Create(e.db, req)
}

// List returns list of equipmentTags
func (e *EquipmentTags) List(c echo.Context, p *symbiomatique.Pagination) ([]symbiomatique.EquipmentTags, error) {
	if err := e.rbac.IsLowerRole(c, symbiomatique.AdminRole); err != nil {
		return nil, err
	}

	return e.edb.List(e.db, p)
}

// View returns single equipmentTags
func (e *EquipmentTags) View(c echo.Context, id int) (*symbiomatique.EquipmentTags, error) {
	if err := e.rbac.IsLowerRole(c, symbiomatique.ContributorRole); err != nil {
		return nil, err
	}

	equipmentTags, err := e.edb.View(e.db, id)
	if err != nil {
		return nil, err
	}
	if err := e.rbac.EnforceUser(c, equipmentTags.CreatedByID); err != nil {
		return nil, err
	}
	return e.edb.View(e.db, id)
}

// Delete deletes a equipmentTags.
func (e *EquipmentTags) Delete(c echo.Context, id int) error {
	if err := e.rbac.IsLowerRole(c, symbiomatique.ContributorRole); err != nil {
		return err
	}

	equipmentTags, err := e.edb.View(e.db, id)
	if err != nil {
		return err
	}
	if err := e.rbac.EnforceUser(c, equipmentTags.CreatedByID); err != nil {
		return err
	}
	return e.edb.Delete(e.db, equipmentTags)
}

// Update contains equipmentTags's information used for updating
type Update struct {
	ID           int
	EquipmentID  int
	TagID        int
	AverageScore float64
}

// Update updates equipmentTags's contact information
func (e *EquipmentTags) Update(c echo.Context, req *Update) (*symbiomatique.EquipmentTags, error) {
	if err := e.rbac.IsLowerRole(c, symbiomatique.ContributorRole); err != nil {
		return nil, err
	}

	equipmentTags, err := e.edb.View(e.db, req.ID)
	if err != nil {
		return nil, err
	}
	if err := e.rbac.EnforceUser(c, equipmentTags.CreatedByID); err != nil {
		return nil, err
	}

	if err := e.edb.Update(e.db, &symbiomatique.EquipmentTags{
		Base:         symbiomatique.Base{ID: req.ID},
		EquipmentID:  req.EquipmentID,
		TagID:        req.TagID,
		AverageScore: req.AverageScore,
	}); err != nil {
		return nil, err
	}

	return equipmentTags, nil
}
