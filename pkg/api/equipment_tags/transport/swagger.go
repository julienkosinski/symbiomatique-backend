package transport

import (
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// EquipmentTags model response
// swagger:response userResp
type swaggEquipmentTagsResponse struct {
	// in:body
	Body struct {
		*symbiomatique.EquipmentTags
	}
}

// EquipmentTags model response
// swagger:response userListResp
type swaggEquipmentTagsListResponse struct {
	// in:body
	Body struct {
		EquipmentTags []symbiomatique.EquipmentTags `json:"equipment_tags"`
		Page          int                           `json:"page"`
	}
}
