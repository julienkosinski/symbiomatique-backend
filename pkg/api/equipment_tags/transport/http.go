package transport

import (
	"net/http"
	"strconv"

	equipmentTags "gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/equipment_tags"

	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"

	"github.com/labstack/echo/v4"
)

// HTTP represents equipmentTags http service
type HTTP struct {
	svc equipmentTags.Service
}

// NewHTTP creates new equipmentTags http service
func NewHTTP(svc equipmentTags.Service, er *echo.Group) {
	h := HTTP{svc}
	ur := er.Group("/equipment_tags")
	// swagger:route POST /v1/equipment_tags equipmentTags equipmentTagsCreate
	// Creates new equipmentTags.
	// responses:
	//  200: equipmentTagsResp
	//  400: errMsg
	//  401: err
	//  403: errMsg
	//  500: err
	ur.POST("", h.create)

	// swagger:operation GET /v1/equipment_tags equipmentTags listEquipmentTags
	// ---
	// summary: Returns list of equipmentTags.
	// description: Returns list of equipmentTags. Depending on the equipmentTags role requesting it, it may return all equipmentTags for SuperAdmin/Admin equipmentTags, all company/location equipmentTags for Company/Location admins, and an error for non-admin equipmentTags.
	// parameters:
	// - name: limit
	//   in: query
	//   description: number of results
	//   type: int
	//   required: false
	// - name: page
	//   in: query
	//   description: page number
	//   type: int
	//   required: false
	// responses:
	//   "200":
	//     "$ref": "#/responses/equipmentTagsListResp"
	//   "400":
	//     "$ref": "#/responses/errMsg"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	ur.GET("", h.list)

	// swagger:operation GET /v1/equipment_tags/{id} equipmentTags getEquipmentTags
	// ---
	// summary: Returns a single equipmentTags.
	// description: Returns a single equipmentTags by its ID.
	// parameters:
	// - name: id
	//   in: path
	//   description: id of equipmentTags
	//   type: int
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/responses/equipmentTagsResp"
	//   "400":
	//     "$ref": "#/responses/err"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "404":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	ur.GET("/:id", h.view)

	// swagger:operation PATCH /v1/equipmentTags/{id} equipmentTags equipmentTagsUpdate
	// ---
	// summary: Updates equipmentTags' information.
	// description: Updates equipmentTags' information.
	// parameters:
	// - name: id
	//   in: path
	//   description: id of equipmentTags
	//   type: int
	//   required: true
	// - name: request
	//   in: body
	//   description: Request body
	//   required: true
	//   schema:
	//     "$ref": "#/definitions/equipmentTagsUpdate"
	// responses:
	//   "200":
	//     "$ref": "#/responses/equipmentTagsResp"
	//   "400":
	//     "$ref": "#/responses/errMsg"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	ur.PATCH("/:id", h.update)

	// swagger:operation DELETE /v1/equipment_tags/{id} equipmentTags equipmentTagsDelete
	// ---
	// summary: Deletes a equipmentTags.
	// description: Deletes a equipmentTags with requested ID.
	// parameters:
	// - name: id
	//   in: path
	//   description: id of equipmentTags
	//   type: int
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/responses/ok"
	//   "400":
	//     "$ref": "#/responses/err"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	ur.DELETE("/:id", h.delete)
}

// EquipmentTags create request
// swagger:model equipmentTagsCreate
type createReq struct {
	EquipmentID  int     `json:"equipment_id" validate:"required"`
	TagID        int     `json:"component_id" validate:"required"`
	AverageScore float64 `json:"average_score" validate:"required"`
}

func (h *HTTP) create(c echo.Context) error {
	r := new(createReq)

	if err := c.Bind(r); err != nil {

		return err
	}

	usr, err := h.svc.Create(c, symbiomatique.EquipmentTags{
		EquipmentID:  r.EquipmentID,
		TagID:        r.TagID,
		AverageScore: r.AverageScore,
	})

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, usr)
}

type listResponse struct {
	EquipmentTags []symbiomatique.EquipmentTags `json:"equipmentTags"`
	Page          int                           `json:"page"`
}

func (h *HTTP) list(c echo.Context) error {
	p := new(symbiomatique.PaginationReq)
	if err := c.Bind(p); err != nil {
		return err
	}

	result, err := h.svc.List(c, p.Transform())

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, listResponse{result, p.Page})
}

func (h *HTTP) view(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return symbiomatique.ErrBadRequest
	}

	result, err := h.svc.View(c, id)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, result)
}

// EquipmentTags update request
// swagger:model equipmentTagsUpdate
type updateReq struct {
	ID           int     `json:"-"`
	EquipmentID  int     `json:"equipment_id"`
	TagID        int     `json:"tag_id"`
	AverageScore float64 `json:"average_score,omitempty"`
}

func (h *HTTP) update(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return symbiomatique.ErrBadRequest
	}

	req := new(updateReq)
	if err := c.Bind(req); err != nil {
		return err
	}

	usr, err := h.svc.Update(c, &equipmentTags.Update{
		ID:           id,
		EquipmentID:  req.EquipmentID,
		TagID:        req.TagID,
		AverageScore: req.AverageScore,
	})

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, usr)
}

func (h *HTTP) delete(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return symbiomatique.ErrBadRequest
	}

	if err := h.svc.Delete(c, id); err != nil {
		return err
	}

	return c.NoContent(http.StatusOK)
}
