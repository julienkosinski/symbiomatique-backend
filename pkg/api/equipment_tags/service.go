package equipmentTags

import (
	"github.com/go-pg/pg/v9"
	"github.com/go-pg/pg/v9/orm"
	"github.com/labstack/echo/v4"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/equipment_tags/platform/pgsql"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// Service represents equipmentTags application interface
type Service interface {
	Create(echo.Context, symbiomatique.EquipmentTags) (*symbiomatique.EquipmentTags, error)
	List(echo.Context, *symbiomatique.Pagination) ([]symbiomatique.EquipmentTags, error)
	View(echo.Context, int) (*symbiomatique.EquipmentTags, error)
	Delete(echo.Context, int) error
	Update(echo.Context, *Update) (*symbiomatique.EquipmentTags, error)
}

// New creates new equipmentTags application service
func New(db *pg.DB, edb EDB, rbac RBAC) *EquipmentTags {
	return &EquipmentTags{db: db, edb: edb, rbac: rbac}
}

// Initialize initializes EquipmentTags application service with defaults
func Initialize(db *pg.DB, rbac RBAC) *EquipmentTags {
	return New(db, pgsql.NewEquipmentTags(), rbac)
}

// EquipmentTags represents equipmentTags application service
type EquipmentTags struct {
	db   *pg.DB
	edb  EDB
	rbac RBAC
}

// EDB represents equipmentTags repository interface
type EDB interface {
	Create(orm.DB, symbiomatique.EquipmentTags) (*symbiomatique.EquipmentTags, error)
	View(orm.DB, int) (*symbiomatique.EquipmentTags, error)
	List(orm.DB, *symbiomatique.Pagination) ([]symbiomatique.EquipmentTags, error)
	Update(orm.DB, *symbiomatique.EquipmentTags) error
	Delete(orm.DB, *symbiomatique.EquipmentTags) error
}

// RBAC represents role-based-access-control interface
type RBAC interface {
	IsLowerRole(echo.Context, symbiomatique.AccessRole) error
	EnforceUser(echo.Context, int) error
}
