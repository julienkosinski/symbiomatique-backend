package pgsql

import (
	"net/http"

	"github.com/go-pg/pg/v9"
	"github.com/go-pg/pg/v9/orm"
	"github.com/labstack/echo/v4"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// NewEquipmentTags returns a new equipmentTags database instance
func NewEquipmentTags() *EquipmentTags {
	return &EquipmentTags{}
}

// EquipmentTags represents the client for equipmentTags table
type EquipmentTags struct{}

// Custom errors
var (
	ErrAlreadyExists = echo.NewHTTPError(http.StatusInternalServerError, "EquipmentTags already exists.")
)

// Create creates a new equipmentTags on database
func (et *EquipmentTags) Create(db orm.DB, eqfe symbiomatique.EquipmentTags) (*symbiomatique.EquipmentTags, error) {
	var equipmentTags = new(symbiomatique.EquipmentTags)
	err := db.Model(equipmentTags).Where("equipment_id = ? and deleted_at is null",
		eqfe.EquipmentID).Where("tag_id = ? and deleted_at is null",
		eqfe.TagID).Select()

	if err != nil && err != pg.ErrNoRows {
		return nil, ErrAlreadyExists

	}

	if err := db.Insert(&eqfe); err != nil {
		return nil, err
	}
	return &eqfe, nil
}

// View returns single equipmentTags by ID
func (et *EquipmentTags) View(db orm.DB, id int) (*symbiomatique.EquipmentTags, error) {
	var equipmentTags = new(symbiomatique.EquipmentTags)
	sql := `SELECT "equipment_tags".* FROM "equipment_tags" WHERE ("equipment_tags"."id" = ? and deleted_at is null)`
	_, err := db.QueryOne(equipmentTags, sql, id)
	if err != nil {
		return nil, err
	}

	return equipmentTags, nil
}

// Update updates equipmentTags's info
func (et *EquipmentTags) Update(db orm.DB, equipmentTags *symbiomatique.EquipmentTags) error {
	_, err := db.Model(equipmentTags).WherePK().UpdateNotZero()
	return err
}

// List returns list of all equipmentTagss retrievable for the current equipmentTags
func (et *EquipmentTags) List(db orm.DB, p *symbiomatique.Pagination) ([]symbiomatique.EquipmentTags, error) {
	var equipmentTags []symbiomatique.EquipmentTags
	q := db.Model(&equipmentTags).Column("equipment_tags.*").Limit(p.Limit).Offset(p.Offset)
	if err := q.Select(); err != nil {
		return nil, err
	}
	return equipmentTags, nil
}

// Delete sets deleted_at for a equipmentTags
func (et *EquipmentTags) Delete(db orm.DB, equipmentTags *symbiomatique.EquipmentTags) error {
	return db.Delete(equipmentTags)
}
