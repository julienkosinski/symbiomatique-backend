package equipmentTags

import (
	"time"

	"github.com/labstack/echo/v4"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/equipment_tags"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// New creates new equipmentTags logging service
func New(svc equipmentTags.Service, logger symbiomatique.Logger) *LogService {
	return &LogService{
		Service: svc,
		logger:  logger,
	}
}

// LogService represents equipmentTags logging service
type LogService struct {
	equipmentTags.Service
	logger symbiomatique.Logger
}

const name = "equipmentTags"

// Create logging
func (ls *LogService) Create(c echo.Context, req symbiomatique.EquipmentTags) (resp *symbiomatique.EquipmentTags, err error) {
	defer func(begin time.Time) {
		ls.logger.Log(
			c,
			name, "Create equipmentTags request", err,
			map[string]interface{}{
				"req":  req,
				"resp": resp,
				"took": time.Since(begin),
			},
		)
	}(time.Now())
	return ls.Service.Create(c, req)
}

// List logging
func (ls *LogService) List(c echo.Context, req *symbiomatique.Pagination) (resp []symbiomatique.EquipmentTags, err error) {
	defer func(begin time.Time) {
		ls.logger.Log(
			c,
			name, "List equipmentTags request", err,
			map[string]interface{}{
				"req":  req,
				"resp": resp,
				"took": time.Since(begin),
			},
		)
	}(time.Now())
	return ls.Service.List(c, req)
}

// View logging
func (ls *LogService) View(c echo.Context, req int) (resp *symbiomatique.EquipmentTags, err error) {
	defer func(begin time.Time) {
		ls.logger.Log(
			c,
			name, "View equipmentTags request", err,
			map[string]interface{}{
				"req":  req,
				"resp": resp,
				"took": time.Since(begin),
			},
		)
	}(time.Now())
	return ls.Service.View(c, req)
}

// Delete logging
func (ls *LogService) Delete(c echo.Context, req int) (err error) {
	defer func(begin time.Time) {
		ls.logger.Log(
			c,
			name, "Delete equipmentTags request", err,
			map[string]interface{}{
				"req":  req,
				"took": time.Since(begin),
			},
		)
	}(time.Now())
	return ls.Service.Delete(c, req)
}

// Update logging
func (ls *LogService) Update(c echo.Context, req *equipmentTags.Update) (resp *symbiomatique.EquipmentTags, err error) {
	defer func(begin time.Time) {
		ls.logger.Log(
			c,
			name, "Update equipmentTags request", err,
			map[string]interface{}{
				"req":  req,
				"resp": resp,
				"took": time.Since(begin),
			},
		)
	}(time.Now())
	return ls.Service.Update(c, req)
}
