package equipmentComponent

import (
	"github.com/go-pg/pg/v9"
	"github.com/go-pg/pg/v9/orm"
	"github.com/labstack/echo/v4"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/equipment_components/platform/pgsql"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// Service represents equipmentComponent application interface
type Service interface {
	Create(echo.Context, symbiomatique.EquipmentComponents) (*symbiomatique.EquipmentComponents, error)
	List(echo.Context, *symbiomatique.Pagination) ([]symbiomatique.EquipmentComponents, error)
	View(echo.Context, int) (*symbiomatique.EquipmentComponents, error)
	Delete(echo.Context, int) error
}

// New creates new equipmentComponent application service
func New(db *pg.DB, edb EDB, rbac RBAC) *EquipmentComponents {
	return &EquipmentComponents{db: db, edb: edb, rbac: rbac}
}

// Initialize initializes EquipmentComponents application service with defaults
func Initialize(db *pg.DB, rbac RBAC) *EquipmentComponents {
	return New(db, pgsql.NewEquipmentComponents(), rbac)
}

// EquipmentComponents represents equipmentComponent application service
type EquipmentComponents struct {
	db   *pg.DB
	edb  EDB
	rbac RBAC
}

// EDB represents equipmentComponent repository interface
type EDB interface {
	Create(orm.DB, symbiomatique.EquipmentComponents) (*symbiomatique.EquipmentComponents, error)
	View(orm.DB, int) (*symbiomatique.EquipmentComponents, error)
	List(orm.DB, *symbiomatique.Pagination) ([]symbiomatique.EquipmentComponents, error)
	Delete(orm.DB, *symbiomatique.EquipmentComponents) error
}

// RBAC represents role-based-access-control interface
type RBAC interface {
	IsLowerRole(echo.Context, symbiomatique.AccessRole) error
	EnforceUser(echo.Context, int) error
}
