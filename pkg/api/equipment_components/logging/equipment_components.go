package equipmentComponent

import (
	"time"

	"github.com/labstack/echo/v4"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/equipment_components"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// New creates new equipmentComponent logging service
func New(svc equipmentComponent.Service, logger symbiomatique.Logger) *LogService {
	return &LogService{
		Service: svc,
		logger:  logger,
	}
}

// LogService represents equipmentComponent logging service
type LogService struct {
	equipmentComponent.Service
	logger symbiomatique.Logger
}

const name = "equipmentComponent"

// Create logging
func (ls *LogService) Create(c echo.Context, req symbiomatique.EquipmentComponents) (resp *symbiomatique.EquipmentComponents, err error) {
	defer func(begin time.Time) {
		ls.logger.Log(
			c,
			name, "Create equipmentComponent request", err,
			map[string]interface{}{
				"req":  req,
				"resp": resp,
				"took": time.Since(begin),
			},
		)
	}(time.Now())
	return ls.Service.Create(c, req)
}

// List logging
func (ls *LogService) List(c echo.Context, req *symbiomatique.Pagination) (resp []symbiomatique.EquipmentComponents, err error) {
	defer func(begin time.Time) {
		ls.logger.Log(
			c,
			name, "List equipmentComponent request", err,
			map[string]interface{}{
				"req":  req,
				"resp": resp,
				"took": time.Since(begin),
			},
		)
	}(time.Now())
	return ls.Service.List(c, req)
}

// View logging
func (ls *LogService) View(c echo.Context, req int) (resp *symbiomatique.EquipmentComponents, err error) {
	defer func(begin time.Time) {
		ls.logger.Log(
			c,
			name, "View equipmentComponent request", err,
			map[string]interface{}{
				"req":  req,
				"resp": resp,
				"took": time.Since(begin),
			},
		)
	}(time.Now())
	return ls.Service.View(c, req)
}

// Delete logging
func (ls *LogService) Delete(c echo.Context, req int) (err error) {
	defer func(begin time.Time) {
		ls.logger.Log(
			c,
			name, "Delete equipmentComponent request", err,
			map[string]interface{}{
				"req":  req,
				"took": time.Since(begin),
			},
		)
	}(time.Now())
	return ls.Service.Delete(c, req)
}
