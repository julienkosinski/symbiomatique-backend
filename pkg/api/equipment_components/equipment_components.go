// Package equipmentComponent contains equipmentComponent application services
package equipmentComponent

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// Create creates a new equipmentComponent account
func (e *EquipmentComponents) Create(c echo.Context, req symbiomatique.EquipmentComponents) (*symbiomatique.EquipmentComponents, error) {
	return e.edb.Create(e.db, req)
}

// List returns list of equipmentComponent
func (e *EquipmentComponents) List(c echo.Context, p *symbiomatique.Pagination) ([]symbiomatique.EquipmentComponents, error) {
	return e.edb.List(e.db, p)
}

// View returns single equipmentComponent
func (e *EquipmentComponents) View(c echo.Context, id int) (*symbiomatique.EquipmentComponents, error) {
	return e.edb.View(e.db, id)
}

// Delete deletes a equipmentComponent
func (e *EquipmentComponents) Delete(c echo.Context, id int) error {
	equipmentComponent, err := e.edb.View(e.db, id)
	if err != nil {
		return err
	}
	return e.edb.Delete(e.db, equipmentComponent)
}
