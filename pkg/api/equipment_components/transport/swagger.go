package transport

import (
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// EquipmentComponents model response
// swagger:response EquipmentComponentsResp
type swaggEquipmentComponentsResponse struct {
	// in:body
	Body struct {
		*symbiomatique.EquipmentComponents
	}
}

// EquipmentComponents model response
// swagger:response EquipmentComponentsListResp
type swaggEquipmentComponentsListResponse struct {
	// in:body
	Body struct {
		EquipmentComponents []symbiomatique.EquipmentComponents `json:"equipment_components"`
		Page                int                                 `json:"page"`
	}
}
