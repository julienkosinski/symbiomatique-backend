package transport

import (
	"net/http"
	"strconv"

	equipmentComponent "gitlab.com/julienkosinski/symbiomatique-backend/pkg/api/equipment_components"

	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"

	"github.com/labstack/echo/v4"
)

// HTTP represents equipmentComponent http service
type HTTP struct {
	svc equipmentComponent.Service
}

// NewHTTP creates new equipmentComponent http service
func NewHTTP(svc equipmentComponent.Service, er *echo.Group) {
	h := HTTP{svc}
	ur := er.Group("/equipment_components")
	// swagger:route POST /v1/equipment_components equipmentComponent equipmentComponentCreate
	// Creates new equipmentComponent.
	// responses:
	//  200: equipmentComponentResp
	//  400: errMsg
	//  401: err
	//  403: errMsg
	//  500: err
	ur.POST("", h.create)

	// swagger:operation GET /v1/equipment_components equipmentComponent listEquipmentComponents
	// ---
	// summary: Returns list of equipmentComponent.
	// description: Returns list of equipmentComponent. Depending on the equipmentComponent role requesting it, it may return all equipmentComponent for SuperAdmin/Admin equipmentComponent, all company/location equipmentComponent for Company/Location admins, and an error for non-admin equipmentComponent.
	// parameters:
	// - name: limit
	//   in: query
	//   description: number of results
	//   type: int
	//   required: false
	// - name: page
	//   in: query
	//   description: page number
	//   type: int
	//   required: false
	// responses:
	//   "200":
	//     "$ref": "#/responses/equipmentComponentListResp"
	//   "400":
	//     "$ref": "#/responses/errMsg"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	ur.GET("", h.list)

	// swagger:operation GET /v1/equipment_components/{id} equipmentComponent getEquipmentComponents
	// ---
	// summary: Returns a single equipmentComponent.
	// description: Returns a single equipmentComponent by its ID.
	// parameters:
	// - name: id
	//   in: path
	//   description: id of equipmentComponent
	//   type: int
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/responses/equipmentComponentResp"
	//   "400":
	//     "$ref": "#/responses/err"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "404":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	ur.GET("/:id", h.view)

	// swagger:operation DELETE /v1/equipment_components/{id} equipmentComponent equipmentComponentDelete
	// ---
	// summary: Deletes an equipmentComponent.
	// description: Deletes a equipmentComponent with requested ID.
	// parameters:
	// - name: id
	//   in: path
	//   description: id of equipmentComponent
	//   type: int
	//   required: true
	// responses:
	//   "200":
	//     "$ref": "#/responses/ok"
	//   "400":
	//     "$ref": "#/responses/err"
	//   "401":
	//     "$ref": "#/responses/err"
	//   "403":
	//     "$ref": "#/responses/err"
	//   "500":
	//     "$ref": "#/responses/err"
	ur.DELETE("/:id", h.delete)
}

// EquipmentComponents create request
// swagger:model equipmentComponentCreate
type createReq struct {
	EquipmentID int `json:"equipment_id" validate:"required"`
	ComponentID int `json:"component_id" validate:"required"`
}

func (h *HTTP) create(c echo.Context) error {
	r := new(createReq)

	if err := c.Bind(r); err != nil {

		return err
	}

	usr, err := h.svc.Create(c, symbiomatique.EquipmentComponents{
		EquipmentID: r.EquipmentID,
		ComponentID: r.ComponentID,
	})

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, usr)
}

type listResponse struct {
	EquipmentComponents []symbiomatique.EquipmentComponents `json:"equipmentComponent"`
	Page                int                                 `json:"page"`
}

func (h *HTTP) list(c echo.Context) error {
	p := new(symbiomatique.PaginationReq)
	if err := c.Bind(p); err != nil {
		return err
	}

	result, err := h.svc.List(c, p.Transform())

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, listResponse{result, p.Page})
}

func (h *HTTP) view(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return symbiomatique.ErrBadRequest
	}

	result, err := h.svc.View(c, id)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, result)
}

func (h *HTTP) delete(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return symbiomatique.ErrBadRequest
	}

	if err := h.svc.Delete(c, id); err != nil {
		return err
	}

	return c.NoContent(http.StatusOK)
}
