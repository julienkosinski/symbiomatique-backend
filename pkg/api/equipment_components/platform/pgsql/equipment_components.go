package pgsql

import (
	"net/http"

	"github.com/go-pg/pg/v9"
	"github.com/go-pg/pg/v9/orm"
	"github.com/labstack/echo/v4"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// NewEquipmentComponents returns a new equipmentComponent database instance
func NewEquipmentComponents() *EquipmentComponents {
	return &EquipmentComponents{}
}

// EquipmentComponents represents the client for equipmentComponent table
type EquipmentComponents struct{}

// Custom errors
var (
	ErrAlreadyExists = echo.NewHTTPError(http.StatusInternalServerError, "EquipmentComponents already exists.")
)

// Create creates a new equipmentComponent on database
func (ec *EquipmentComponents) Create(db orm.DB, eqco symbiomatique.EquipmentComponents) (*symbiomatique.EquipmentComponents, error) {
	var equipmentComponent = new(symbiomatique.EquipmentComponents)
	err := db.Model(equipmentComponent).Where("equipment_id = ? and deleted_at is null",
		eqco.EquipmentID).Where("component_id = ? and deleted_at is null",
		eqco.ComponentID).Select()

	if err != nil && err != pg.ErrNoRows {
		return nil, ErrAlreadyExists

	}

	if err := db.Insert(&eqco); err != nil {
		return nil, err
	}
	return &eqco, nil
}

// View returns single equipmentComponent by ID
func (ec *EquipmentComponents) View(db orm.DB, id int) (*symbiomatique.EquipmentComponents, error) {
	var equipmentComponent = new(symbiomatique.EquipmentComponents)
	sql := `SELECT "equipment_components".* FROM "equipment_components" WHERE ("equipment_components"."id" = ? and deleted_at is null)`
	_, err := db.QueryOne(equipmentComponent, sql, id)
	if err != nil {
		return nil, err
	}

	return equipmentComponent, nil
}

// List returns list of all equipmentComponents retrievable for the current equipmentComponent
func (ec *EquipmentComponents) List(db orm.DB, p *symbiomatique.Pagination) ([]symbiomatique.EquipmentComponents, error) {
	var equipmentComponent []symbiomatique.EquipmentComponents
	q := db.Model(&equipmentComponent).Column("equipment_components.*").Limit(p.Limit).Offset(p.Offset)
	if err := q.Select(); err != nil {
		return nil, err
	}
	return equipmentComponent, nil
}

// Delete sets deleted_at for a equipmentComponent
func (ec *EquipmentComponents) Delete(db orm.DB, equipmentComponent *symbiomatique.EquipmentComponents) error {
	return db.Delete(equipmentComponent)
}
