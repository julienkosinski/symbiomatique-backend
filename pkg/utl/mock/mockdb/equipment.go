package mockdb

import (
	"github.com/go-pg/pg/v9"
	"github.com/go-pg/pg/v9/orm"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// Equipment database mock
type Equipment struct {
	CreateFn         func(orm.DB, symbiomatique.Equipment) (*symbiomatique.Equipment, error)
	ViewFn           func(orm.DB, int) (*symbiomatique.Equipment, error)
	ListFn           func(orm.DB, *symbiomatique.Pagination) ([]symbiomatique.Equipment, error)
	DeleteFn         func(orm.DB, *symbiomatique.Equipment) error
	UpdateFn         func(orm.DB, *symbiomatique.Equipment) error
	AverageScoreFn   func(orm.DB, int) error
	CheckBestMatchFn func(*pg.DB, []float64) (*symbiomatique.Equipment, error)
}

// AverageScore re-average score mock
func (e *Equipment) AverageScore(db orm.DB, id int) error {
	return e.AverageScoreFn(db, id)
}

// CheckBestMatch mock
func (e *Equipment) CheckBestMatch(db *pg.DB, wantedAverageScores []float64) (*symbiomatique.Equipment, error) {
	return e.CheckBestMatch(db, wantedAverageScores)
}

// Create mock
func (e *Equipment) Create(db orm.DB, eq symbiomatique.Equipment) (*symbiomatique.Equipment, error) {
	return e.CreateFn(db, eq)
}

// View mock
func (e *Equipment) View(db orm.DB, id int) (*symbiomatique.Equipment, error) {
	return e.ViewFn(db, id)
}

// List mock
func (e *Equipment) List(db orm.DB, p *symbiomatique.Pagination) ([]symbiomatique.Equipment, error) {
	return e.ListFn(db, p)
}

// Delete mock
func (e *Equipment) Delete(db orm.DB, eq *symbiomatique.Equipment) error {
	return e.DeleteFn(db, eq)
}

// Update mock
func (e *Equipment) Update(db orm.DB, eq *symbiomatique.Equipment) error {
	return e.UpdateFn(db, eq)
}
