package mockdb

import (
	"github.com/go-pg/pg/v9/orm"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// User database mock
type User struct {
	CreateFn         func(orm.DB, symbiomatique.User) (*symbiomatique.User, error)
	ViewFn           func(orm.DB, int) (*symbiomatique.User, error)
	FindByUsernameFn func(orm.DB, string) (*symbiomatique.User, error)
	FindByTokenFn    func(orm.DB, string) (*symbiomatique.User, error)
	ListFn           func(orm.DB, *symbiomatique.ListQuery, *symbiomatique.Pagination) ([]symbiomatique.User, error)
	DeleteFn         func(orm.DB, *symbiomatique.User) error
	UpdateFn         func(orm.DB, *symbiomatique.User) error
}

// Create mock
func (u *User) Create(db orm.DB, usr symbiomatique.User) (*symbiomatique.User, error) {
	return u.CreateFn(db, usr)
}

// View mock
func (u *User) View(db orm.DB, id int) (*symbiomatique.User, error) {
	return u.ViewFn(db, id)
}

// FindByUsername mock
func (u *User) FindByUsername(db orm.DB, uname string) (*symbiomatique.User, error) {
	return u.FindByUsernameFn(db, uname)
}

// FindByToken mock
func (u *User) FindByToken(db orm.DB, token string) (*symbiomatique.User, error) {
	return u.FindByTokenFn(db, token)
}

// List mock
func (u *User) List(db orm.DB, lq *symbiomatique.ListQuery, p *symbiomatique.Pagination) ([]symbiomatique.User, error) {
	return u.ListFn(db, lq, p)
}

// Delete mock
func (u *User) Delete(db orm.DB, usr *symbiomatique.User) error {
	return u.DeleteFn(db, usr)
}

// Update mock
func (u *User) Update(db orm.DB, usr *symbiomatique.User) error {
	return u.UpdateFn(db, usr)
}
