package mock

import (
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// JWT mock
type JWT struct {
	GenerateTokenFn func(*symbiomatique.User) (string, string, error)
}

// GenerateToken mock
func (j *JWT) GenerateToken(u *symbiomatique.User) (string, string, error) {
	return j.GenerateTokenFn(u)
}
