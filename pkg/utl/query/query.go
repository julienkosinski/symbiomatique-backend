package query

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// List prepares data for list queries
func List(u *symbiomatique.AuthUser) (*symbiomatique.ListQuery, error) {
	switch true {
	case u.Role <= symbiomatique.AdminRole: // user is SuperAdmin or Admin
		return nil, nil
	case u.Role == symbiomatique.CompanyAdminRole:
		return &symbiomatique.ListQuery{Query: "company_id = ?", ID: u.CompanyID}, nil
	case u.Role == symbiomatique.LocationAdminRole:
		return &symbiomatique.ListQuery{Query: "location_id = ?", ID: u.LocationID}, nil
	default:
		return nil, echo.ErrForbidden
	}
}
