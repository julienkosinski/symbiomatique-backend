package query_test

import (
	"testing"

	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"

	"github.com/labstack/echo/v4"

	"github.com/stretchr/testify/assert"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/query"
)

func TestList(t *testing.T) {
	type args struct {
		user *symbiomatique.AuthUser
	}
	cases := []struct {
		name     string
		args     args
		wantData *symbiomatique.ListQuery
		wantErr  error
	}{
		{
			name: "Super admin user",
			args: args{user: &symbiomatique.AuthUser{
				Role: symbiomatique.SuperAdminRole,
			}},
		},
		{
			name: "Company admin user",
			args: args{user: &symbiomatique.AuthUser{
				Role:      symbiomatique.CompanyAdminRole,
				CompanyID: 1,
			}},
			wantData: &symbiomatique.ListQuery{
				Query: "company_id = ?",
				ID:    1},
		},
		{
			name: "Location admin user",
			args: args{user: &symbiomatique.AuthUser{
				Role:       symbiomatique.LocationAdminRole,
				CompanyID:  1,
				LocationID: 2,
			}},
			wantData: &symbiomatique.ListQuery{
				Query: "location_id = ?",
				ID:    2},
		},
		{
			name: "Normal user",
			args: args{user: &symbiomatique.AuthUser{
				Role: symbiomatique.UserRole,
			}},
			wantErr: echo.ErrForbidden,
		},
	}
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			q, err := query.List(tt.args.user)
			assert.Equal(t, tt.wantData, q)
			assert.Equal(t, tt.wantErr, err)
		})
	}
}
