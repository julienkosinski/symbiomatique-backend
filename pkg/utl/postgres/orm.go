package postgres

import (
	"github.com/go-pg/pg/v9/orm"
	symbiomatique "gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
)

// Keeps using singular table names
func init() {
	orm.SetTableNameInflector(func(s string) string {
		return s
	})

	orm.RegisterTable((*symbiomatique.EquipmentComponents)(nil))
	orm.RegisterTable((*symbiomatique.EquipmentTags)(nil))
	orm.RegisterTable((*symbiomatique.Score)(nil))
}
