package symbiomatique

import (
	"github.com/go-pg/pg/v9/orm"
)

// EquipmentTags
type EquipmentTags struct {
	Base
	EquipmentID  int `json:"equipment_id"`
	Equipment    Equipment
	TagID        int `json:"tag_id"`
	Tag          Tag
	AverageScore float64 `json:"average_score"`
}

// EquipmentTagsDB represents EquipmentTags database interface (repository)
type EquipmentTagsDB interface {
	Create(orm.DB, *EquipmentTags) error
	View(orm.DB, int) (*EquipmentTags, error)
	List(orm.DB, *Pagination) ([]EquipmentTags, error)
	Delete(orm.DB, *EquipmentTags) error
	Update(orm.DB, *EquipmentTags) (*EquipmentTags, error)
}
