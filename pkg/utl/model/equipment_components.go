package symbiomatique

import (
	"github.com/go-pg/pg/v9/orm"
)

// EquipmentComponents
type EquipmentComponents struct {
	Base
	EquipmentID int
	ComponentID int
}

// EquipmentComponentsDB represents EquipmentComponents database interface (repository)
type EquipmentComponentsDB interface {
	Create(orm.DB, *EquipmentComponents) error
	View(orm.DB, int) (*EquipmentComponents, error)
	List(orm.DB, *Pagination) ([]EquipmentComponents, error)
	Delete(orm.DB, *EquipmentComponents) error
	Update(orm.DB, *EquipmentComponents) (*EquipmentComponents, error)
}
