package symbiomatique

import (
	"context"
	"github.com/go-pg/pg/v9/orm"
	"time"
)

// UserIDCtx is filled with the user ID from JWT middleware
var UserIDCtx interface{}

// Base contains common fields for all tables
type Base struct {
	ID          int       `json:"id"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
	DeletedAt   time.Time `json:"deleted_at,omitempty" pg:",softdbdelete"`
	CreatedByID int       `json:"created_by_id,omitempty"`
	UpdatedByID int       `json:"updated_by_id,omitempty"`
	DeletedByID int       `json:"deleted_by_id,omitempty"`
}

// ListQuery holds company/location data used for list db queries
type ListQuery struct {
	Query string
	ID    int
}

var _ orm.BeforeInsertHook = (*Base)(nil)
var _ orm.BeforeUpdateHook = (*Base)(nil)
var _ orm.BeforeDeleteHook = (*Base)(nil)

// BeforeInsert hooks into insert operations, setting createdAt and updatedAt to current time
func (b *Base) BeforeInsert(ctx context.Context) (context.Context, error) {
	now := time.Now()
	b.CreatedAt = now
	b.UpdatedAt = now

	v := UserIDCtx
	if v != nil {
		userID := v.(int)
		b.CreatedByID = userID
		b.UpdatedByID = userID
	}
	return ctx, nil
}

// BeforeUpdate hooks into update operations, setting updatedAt to current time
func (b *Base) BeforeUpdate(ctx context.Context) (context.Context, error) {
	b.UpdatedAt = time.Now()

	v := UserIDCtx
	if v != nil {
		userID := v.(int)
		b.UpdatedByID = userID
	}
	return ctx, nil
}

// BeforeDelete hooks into delete operations
func (b *Base) BeforeDelete(ctx context.Context) (context.Context, error) {
	v := UserIDCtx
	if v != nil {
		userID := v.(int)
		b.DeletedByID = userID
	}
	return ctx, nil
}
