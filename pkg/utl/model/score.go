package symbiomatique

import "github.com/go-pg/pg/v9/orm"

// Score
type Score struct {
	Base
	EquipmentID int `json:"equipment_id"`
	Equipment   Equipment
	TagID       int `json:"tag_id"`
	Tag         Tag
	Score       float64 `json:"score"`
}

// ScoreDB represents Score database interface (repository)
type ScoreDB interface {
	Create(orm.DB, *Score) error
	View(orm.DB, int) (*Score, error)
	List(orm.DB, *Pagination) ([]Score, error)
	Delete(orm.DB, *Score) error
	Update(orm.DB, *Score) (*Score, error)
}
