package symbiomatique

import (
	"github.com/go-pg/pg/v9/orm"
)

// Equipment
type Equipment struct {
	Base
	Name  string  `json:"name" pg:",type:varchar(50),notnull"`
	IsOld bool    `json:"is_old"`
	Price float64 `json:"price" pg:",notnull"`

	Tags       []Tag       `json:"tags" pg:",many2many:equipment_tags"`
	Components []Component `json:"components" pg:",many2many:equipment_components"`
}

// EquipmentDB represents Equipment database interface (repository)
type EquipmentDB interface {
	Create(orm.DB, *Equipment) error
	View(orm.DB, int) (*Equipment, error)
	List(orm.DB, *Pagination) ([]Equipment, error)
	Delete(orm.DB, *Equipment) error
	Update(orm.DB, *Equipment) (*Equipment, error)
}
