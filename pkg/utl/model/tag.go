package symbiomatique

import (
	"github.com/go-pg/pg/v9/orm"
)

// Tag
type Tag struct {
	Base
	Name string `json:"name" pg:",type:varchar(50),notnull"`

	ListEquipment []Equipment `json:"-" pg:",many2many:equipment_tags"`
}

// TagDB represents Tag database interface (repository)
type TagDB interface {
	Create(orm.DB, *Tag) error
	View(orm.DB, int) (*Tag, error)
	List(orm.DB, *Pagination) ([]Tag, error)
	Delete(orm.DB, *Tag) error
	Update(orm.DB, *Tag) (*Tag, error)
}
