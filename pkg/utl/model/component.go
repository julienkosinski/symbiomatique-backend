package symbiomatique

import (
	"github.com/go-pg/pg/v9/orm"
)

// Component
type Component struct {
	Base
	Name string `json:"name" pg:",type:varchar(50),notnull"`

	ListEquipment []Equipment `json:"all_equipment" pg:",many2many:equipment_components"`
}

// ComponentDB represents Component database interface (repository)
type ComponentDB interface {
	Create(orm.DB, *Component) error
	View(orm.DB, int) (*Component, error)
	List(orm.DB, *Pagination) ([]Component, error)
	Delete(orm.DB, *Component) error
	Update(orm.DB, *Component) (*Component, error)
}
