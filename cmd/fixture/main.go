package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	symbiomatique "gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/model"
	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/secure"

	"github.com/go-pg/pg/v9"
	"github.com/go-pg/pg/v9/orm"
)

// Keeps using singular table names
func init() {
	orm.SetTableNameInflector(func(s string) string {
		return s
	})
}

func main() {
	var psn = `postgres://postgres:admin@postgres:5432/symbiomatique?sslmode=disable`
	u, err := pg.ParseURL(psn)
	checkErr(err)
	db := pg.Connect(u)

	_, err = db.Exec("SELECT 1")
	checkErr(err)

	// If table equipment has data, don't run the script
	equipmentHasData, err := db.Exec("SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = 'equipment'")

	checkErr(err)

	if equipmentHasData.RowsReturned() != 0 {
		os.Exit(0)
	}

	orm.RegisterTable((*symbiomatique.EquipmentComponents)(nil))
	orm.RegisterTable((*symbiomatique.EquipmentTags)(nil))
	orm.RegisterTable((*symbiomatique.Score)(nil))

	createSchema(db, &symbiomatique.Company{}, &symbiomatique.Location{}, &symbiomatique.Role{}, &symbiomatique.User{}, &symbiomatique.Equipment{}, &symbiomatique.Component{}, &symbiomatique.Tag{}, &symbiomatique.EquipmentTags{}, &symbiomatique.EquipmentComponents{}, &symbiomatique.Score{}, &symbiomatique.EquipmentTags{})

	dbInsert := `INSERT INTO public.company VALUES (DEFAULT, now(), now(), NULL, NULL, NULL, NULL, 'admin_company', true);
	INSERT INTO public.location VALUES (DEFAULT, now(), now(), NULL, NULL, NULL, NULL, 'admin_location', true, 'admin_address', 1);
	INSERT INTO public.role VALUES (100, 100, 'SUPER_ADMIN');
	INSERT INTO public.role VALUES (110, 110, 'ADMIN');
	INSERT INTO public.role VALUES (120, 120, 'COMPANY_ADMIN');
	INSERT INTO public.role VALUES (130, 130, 'LOCATION_ADMIN');
	INSERT INTO public.role VALUES (140, 140, 'CONTRIBUTOR');
	INSERT INTO public.role VALUES (200, 200, 'USER');
	INSERT INTO public.tag (id, name) VALUES (DEFAULT, 'Gaming');
	INSERT INTO public.tag (id, name) VALUES (DEFAULT, 'Bureautique');
	INSERT INTO public.tag (id, name) VALUES (DEFAULT, 'Montage vidéo');
	INSERT INTO public.equipment (id, name, is_old, price) VALUES (DEFAULT, 'DELL XPS 13 9370', false, 1400);
	INSERT INTO public.equipment (id, name, is_old, price) VALUES (DEFAULT, 'ASUS VivoBook E203MA-FD017TS', false, 270);
	INSERT INTO public.equipment (id, name, is_old, price) VALUES (DEFAULT, 'ASUS ZenBook Pro Duo UX581GV-H2001', false, 4920);
	INSERT INTO public.equipment (id, name, is_old, price) VALUES (DEFAULT, 'ACER Predator Helios 300 PH317-53-79N6', false, 1650);
	INSERT INTO public.component (id, name) VALUES (DEFAULT, 'Intel Core i5-8250U');
	INSERT INTO public.component (id, name) VALUES (DEFAULT, '8 Go DDR3 1866 Mhz');
	INSERT INTO public.component (id, name) VALUES (DEFAULT, 'Intel UHD Graphics 620');
	INSERT INTO public.component (id, name) VALUES (DEFAULT, '3840 x 2160 pixels');
	INSERT INTO public.component (id, name) VALUES (DEFAULT, 'SSD 256 Go M.2');
	INSERT INTO public.component (id, name) VALUES (DEFAULT, 'SSD 1 To M.2');
	INSERT INTO public.component (id, name) VALUES (DEFAULT, 'Intel Core i9-9980HK');
	INSERT INTO public.component (id, name) VALUES (DEFAULT, '32 Go DDR4 2666 Mhz');
	INSERT INTO public.component (id, name) VALUES (DEFAULT, 'NVIDIA GeForce RTX 2060');
	INSERT INTO public.component (id, name) VALUES (DEFAULT, 'Wi-Fi 6 AX');
	INSERT INTO public.component (id, name) VALUES (DEFAULT, 'Double écran ViewMax');
	INSERT INTO public.component (id, name) VALUES (DEFAULT, 'Intel Celeron N4000');
	INSERT INTO public.component (id, name) VALUES (DEFAULT, '4 Go DDR4');
	INSERT INTO public.component (id, name) VALUES (DEFAULT, '1366 x 768 pixels');
	INSERT INTO public.component (id, name) VALUES (DEFAULT, 'eMMC 64 Go');
	INSERT INTO public.component (id, name) VALUES (DEFAULT, 'Intel UHD Graphics 600');
	INSERT INTO public.component (id, name) VALUES (DEFAULT, 'Intel Core i7-9750H');
	INSERT INTO public.component (id, name) VALUES (DEFAULT, '16 Go DDR4 2666 Mhz');
	INSERT INTO public.component (id, name) VALUES (DEFAULT, '1920 x 1080 pixels');
	INSERT INTO public.equipment_components (id, equipment_id, component_id) VALUES (DEFAULT, 1, 1);
	INSERT INTO public.equipment_components (id, equipment_id, component_id) VALUES (DEFAULT, 1, 2);
	INSERT INTO public.equipment_components (id, equipment_id, component_id) VALUES (DEFAULT, 1, 3);
	INSERT INTO public.equipment_components (id, equipment_id, component_id) VALUES (DEFAULT, 1, 4);
	INSERT INTO public.equipment_components (id, equipment_id, component_id) VALUES (DEFAULT, 3, 4);
	INSERT INTO public.equipment_components (id, equipment_id, component_id) VALUES (DEFAULT, 3, 6);
	INSERT INTO public.equipment_components (id, equipment_id, component_id) VALUES (DEFAULT, 3, 7);
	INSERT INTO public.equipment_components (id, equipment_id, component_id) VALUES (DEFAULT, 3, 8);
	INSERT INTO public.equipment_components (id, equipment_id, component_id) VALUES (DEFAULT, 3, 9);
	INSERT INTO public.equipment_components (id, equipment_id, component_id) VALUES (DEFAULT, 3, 10);
	INSERT INTO public.equipment_components (id, equipment_id, component_id) VALUES (DEFAULT, 3, 11);
	INSERT INTO public.equipment_components (id, equipment_id, component_id) VALUES (DEFAULT, 2, 12);
	INSERT INTO public.equipment_components (id, equipment_id, component_id) VALUES (DEFAULT, 2, 13);
	INSERT INTO public.equipment_components (id, equipment_id, component_id) VALUES (DEFAULT, 2, 14);
	INSERT INTO public.equipment_components (id, equipment_id, component_id) VALUES (DEFAULT, 2, 15);
	INSERT INTO public.equipment_components (id, equipment_id, component_id) VALUES (DEFAULT, 2, 16);
	INSERT INTO public.equipment_components (id, equipment_id, component_id) VALUES (DEFAULT, 4, 17);
	INSERT INTO public.equipment_components (id, equipment_id, component_id) VALUES (DEFAULT, 4, 18);
	INSERT INTO public.equipment_components (id, equipment_id, component_id) VALUES (DEFAULT, 4, 19);
	INSERT INTO public.equipment_components (id, equipment_id, component_id) VALUES (DEFAULT, 4, 6);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 1, 1, 60);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 1, 2, 10);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 1, 3, 70);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 1, 1, 50);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 1, 2, 6);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 1, 3, 80);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 1, 1, 70);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 1, 2, 12);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 1, 3, 70);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 2, 1, 15);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 2, 2, 90);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 2, 3, 20);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 2, 1, 10);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 2, 2, 95);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 2, 2, 80);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 2, 3, 5);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 3, 1, 70);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 3, 2, 0);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 3, 3, 100);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 3, 1, 90);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 3, 2, 30);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 3, 3, 100);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 4, 1, 90);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 4, 2, 20);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 4, 3, 70);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 4, 1, 95);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 4, 2, 15);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 4, 3, 80);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 4, 1, 97);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 4, 2, 10);
	INSERT INTO public.score (id, equipment_id, tag_id, score) VALUES (DEFAULT, 4, 3, 70);`

	queries := strings.Split(dbInsert, ";")

	for _, v := range queries[0 : len(queries)-1] {
		_, err = db.Exec(v)
		checkErr(err)
	}

	var dbAddIndexes = `
	CREATE INDEX index_score ON public.score (equipment_id, tag_id);
	CREATE INDEX index_equipment_tags ON public.equipment_tags (equipment_id, tag_id);
	`

	queries = strings.Split(dbAddIndexes, ";")

	for _, v := range queries[0 : len(queries)-1] {
		_, err = db.Exec(v)
		checkErr(err)
	}

	sec := secure.New(1, nil)

	userInsert := `INSERT INTO public.user (id, created_at, updated_at, first_name, last_name, username, password, email, active, role_id, company_id, location_id) VALUES (DEFAULT, now(),now(),'Admin', 'Admin', 'admin', '%s', 'johndoe@mail.com', true, 100, 1, 1);`
	_, err = db.Exec(fmt.Sprintf(userInsert, sec.Hash("admin")))
	checkErr(err)
	os.Exit(0)
}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func createSchema(db *pg.DB, models ...interface{}) {
	for _, model := range models {
		checkErr(db.CreateTable(model, &orm.CreateTableOptions{
			FKConstraints: true,
			IfNotExists:   true,
		}))
	}
}
