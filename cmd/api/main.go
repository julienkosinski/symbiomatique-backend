package main

import (
	"path"
	"runtime"

	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/api"

	"gitlab.com/julienkosinski/symbiomatique-backend/pkg/utl/config"
)

func main() {
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		panic("No caller information")
	}
	cfgPath := path.Join(path.Dir(filename), "./conf.docker.local.yaml")

	cfg, err := config.Load(cfgPath)
	checkErr(err)

	checkErr(api.Start(cfg))
}

func checkErr(err error) {
	if err != nil {
		panic(err.Error())
	}
}
